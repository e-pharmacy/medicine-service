INSERT INTO classifications (name)
VALUES ('Prescription'),
       ('Restricted'),
       ('Pharmacy only'),
       ('General sale'),
       ('Class A Controlled Drug'),
       ('Class B1 Controlled Drug'),
       ('Class B2 Controlled Drug'),
       ('Class B3 Controlled Drug'),
       ('Class C1 Controlled Drug'),
       ('Class C2 Controlled Drug'),
       ('Class C3 Controlled Drug'),
       ('Class C4 Controlled Drug'),
       ('Class C5 Controlled Drug'),
       ('Class C6 Controlled Drug'),
       ('Class C7 Controlled Drug'),
       ('Temporary Class Drug');

INSERT INTO categories (name, description)
VALUES ('Analgesics',
        'Drugs that relieve pain. There are two main types: non-narcotic analgesics for mild pain, and narcotic analgesics for severe pain.'),
       ('Antacids', 'Drugs that relieve indigestion and heartburn by neutralizing stomach acid.'),
       ('Antianxiety Drugs',
        'Drugs that suppress anxiety and relax muscles (sometimes called anxiolytics, sedatives, or minor tranquilizers).'),
       ('Antiarrhythmics', 'Drugs used to control irregularities of heartbeat.'),
       ('Antibacterials', 'Drugs used to treat infections.'),
       ('Antibiotics',
        'Drugs made from naturally occurring and synthetic substances that combat bacterial infection. Some antibiotics are effective only against limited types of bacteria. Others, known as broad spectrum antibiotics, are effective against a wide range of bacteria.'),
       ('Anticoagulants and Thrombolytics',
        'Anticoagulants prevent blood from clotting. Thrombolytics help dissolve and disperse blood clots and may be prescribed for patients with recent arterial or venous thrombosis.'),
       ('Anticonvulsants', 'Drugs that prevent epileptic seizures.'),
       ('Antidepressants',
        'There are three main groups of mood-lifting antidepressants: tricyclics, monoamine oxidase inhibitors, and selective serotonin reuptake inhibitors (SSRIs).'),
       ('Antidiarrheals',
        'Drugs used for the relief of diarrhea. Two main types of antidiarrheal preparations are simple adsorbent substances and drugs that slow down the contractions of the bowel muscles so that the contents are propelled more slowly.'),
       ('Antiemetics', 'Drugs used to treat nausea and vomiting.'),
       ('Antifungals',
        'Drugs used to treat fungal infections, the most common of which affect the hair, skin, nails, or mucous membranes.'),
       ('Antihistamines',
        'Drugs used primarily to counteract the effects of histamine, one of the chemicals involved in allergic reactions.'),
       ('Antihypertensives',
        'Drugs that lower blood pressure. The types of antihypertensives currently marketed include diuretics, beta-blockers, calcium channel blocker, ACE (angiotensin- converting enzyme) inhibitors, centrally acting antihypertensives and sympatholytics.'),
       ('Anti-Inflammatories',
        'Drugs used to reduce inflammation - the redness, heat, swelling, and increased blood flow found in infections and in many chronic noninfective diseases such as rheumatoid arthritis and gout.'),
       ('Antineoplastics', 'Drugs used to treat cancer.'),
       ('Antipsychotics',
        'Drugs used to treat symptoms of severe psychiatric disorders. These drugs are sometimes called major tranquilizers.'),
       ('Antipyretics', 'Drugs that reduce fever.'),
       ('Antivirals',
        'Drugs used to treat viral infections or to provide temporary protection against infections such as influenza.'),
       ('Barbiturates', 'See "”sleeping drugs."”'),
       ('Beta-Blockers',
        'Beta-adrenergic blocking agents, or beta-blockers for short, reduce the oxygen needs of the heart by reducing heartbeat rate.'),
       ('Bronchodilators',
        'Drugs that open up the bronchial tubes within the lungs when the tubes have become narrowed by muscle spasm. Bronchodilators ease breathing in diseases such as asthma.'),
       ('Cold Cures',
        'Although there is no drug that can cure a cold, the aches, pains, and fever that accompany a cold can be relieved by aspirin or acetaminophen often accompanied by a decongestant, antihistamine, and sometimes caffeine.'),
       ('Corticosteroids',
        'These hormonal preparations are used primarily as anti-inflammatories in arthritis or asthma or as immunosuppressives, but they are also useful for treating some malignancies or compensating for a deficiency of natural hormones in disorders such as Addison''s disease.'),
       ('Cough Suppressants',
        'Simple cough medicines, which contain substances such as honey, glycerine, or menthol, soothe throat irritation but do not actually suppress coughing. They are most soothing when taken as lozenges and dissolved in the mouth. As liquids they are probably swallowed too quickly to be effective. A few drugs are actually cough suppressants. There are two groups of cough suppressants: those that alter the consistency or production of phlegm such as mucolytics and expectorants;; and those that suppress the coughing reflex such as codeine (narcotic cough suppressants), antihistamines, dextromethorphan and isoproterenol (non-narcotic cough suppressants).'),
       ('Cytotoxics',
        'Drugs that kill or damage cells. Cytotoxics are used as antineoplastics (drugs used to treat cancer) and also as immunosuppressives.'),
       ('Decongestants',
        'Drugs that reduce swelling of the mucous membranes that line the nose by constricting blood vessels, thus relieving nasal stuffiness.'),
       ('Diuretics',
        'Drugs that increase the quantity of urine produced by the kidneys and passed out of the body, thus ridding the body of excess fluid. Diuretics reduce water logging of the tissues caused by fluid retention in disorders of the heart, kidneys, and liver. They are useful in treating mild cases of high blood pressure.'),
       ('Expectorant',
        'A drug that stimulates the flow of saliva and promotes coughing to eliminate phlegm from the respiratory tract.'),
       ('Hormones',
        'Chemicals produced naturally by the endocrine glands (thyroid, adrenal, ovary, testis, pancreas, parathyroid). In some disorders, for example, diabetes mellitus, in which too little of a particular hormone is produced, synthetic equivalents or natural hormone extracts are prescribed to restore the deficiency. Such treatment is known as hormone replacement therapy.'),
       ('Hypoglycemics (Oral)',
        'Drugs that lower the level of glucose in the blood. Oral hypoglycemic drugs are used in diabetes mellitus if it cannot be controlled by diet alone, but does require treatment with injections of insulin.'),
       ('Immunosuppressives',
        'Drugs that prevent or reduce the body''s normal reaction to invasion by disease or by foreign tissues. Immunosuppressives are used to treat autoimmune diseases (in which the body''s defenses work abnormally and attack its own tissues) and to help prevent rejection of organ transplants.'),
       ('Laxatives',
        'Drugs that increase the frequency and ease of bowel movements, either by stimulating the bowel wall (stimulant laxative), by increasing the bulk of bowel contents (bulk laxative), or by lubricating them (stool-softeners, or bowel movement-softeners). Laxatives may be taken by mouth or directly into the lower bowel as suppositories or enemas. If laxatives are taken regularly, the bowels may ultimately become unable to work properly without them.'),
       ('Muscle Relaxants',
        'Drugs that relieve muscle spasm in disorders such as backache. Antianxiety drugs (minor tranquilizers) that also have a muscle-relaxant action are used most commonly.'),
       ('Sedatives', 'Same as Antianxiety drugs.'),
       ('Sex Hormones (Female)',
        'There are two groups of these hormones (estrogens and progesterone), which are responsible for development of female secondary sexual characteristics. Small quantities are also produced in males. As drugs, female sex hormones are used to treat menstrual and menopausal disorders and are also used as oral contraceptives. Estrogens may be used to treat cancer of the breast or prostate, progestins (synthetic progesterone to treat endometriosis).'),
       ('Sex Hormones (Male)',
        'Androgenic hormones, of which the most powerful is testosterone, are responsible for development of male secondary sexual characteristics. Small quantities are also produced in females. As drugs, male sex hormones are given to compensate for hormonal deficiency in hypopituitarism or disorders of the testes. They may be used to treat breast cancer in women, but either synthetic derivatives called anabolic steroids, which have less marked side- effects, or specific anti-estrogens are often preferred. Anabolic steroids also have a "body building" effect that has led to their (usually nonsanctioned) use in competitive sports, for both men and women.'),
       ('Sleeping Drugs',
        'The two main groups of drugs that are used to induce sleep are benzodiazepines and barbiturates. All such drugs have a sedative effect in low doses and are effective sleeping medications in higher doses. Benzodiazepines drugs are used more widely than barbiturates because they are safer, the side-effects are less marked, and there is less risk of eventual physical dependence.'),
       ('Tranquilizer',
        'This is a term commonly used to describe any drug that has a calming or sedative effect. However, the drugs that are sometimes called minor tranquilizers should be called antianxiety drugs, and the drugs that are sometimes called major tranquilizers should be called antipsychotics.'),
       ('Vitamins',
        'Chemicals essential in small quantities for good health. Some vitamins are not manufactured by the body, but adequate quantities are present in a normal diet. People whose diets are inadequate or who have digestive tract or liver disorders may need to take supplementary vitamins.'),
       ('Aminopenicillins',
        'Aminopenicillins are bactericidal beta-lactam antibiotics, which work by inhibiting bacterial cell wall synthesis. They are chemically similar to penicillin but have a broader spectrum of activity than penicillin. Aminopenicillins are not deactivated by acid hydrolysis so they can be administered orally, they are however susceptible to hydrolysis by beta-lactamase and therefore are sometimes given with beta-lactamase inhibitors. Aminopenicillins are effective against most gram-positive bacterial infections and gram-negative infections such as E.coli and H.influenza. They are used to treat upper and lower respiratory tract infections, endocarditis urinary tract infections, skin infections, and so on.');

INSERT INTO types (name, description)
VALUES ('Liquid',
        'The active part of the medicine is combined with a liquid to make it easier to take or better absorbed. A liquid may also be called a ''mixture'', ''solution'' or ''syrup''. Many common liquids are now available without any added colouring or sugar.'),
       ('Tablet',
        'The active ingredient is combined with another substance and pressed into a round or oval solid shape. There are different types of tablet. Soluble or dispersible tablets can safely be dissolved in water.'),
       ('Capsules',
        'The active part of the medicine is contained inside a plastic shell that dissolves slowly in the stomach. You can take some capsules apart and mix the contents with your child''s favourite food. Others need to be swallowed whole, so the medicine isn''t absorbed until the stomach acid breaks down the capsule shell.'),
       ('Topical medicines',
        'These are creams, lotions or ointments applied directly onto the skin. They come in tubs, bottles or tubes depending on the type of medicine. The active part of the medicine is mixed with another substance, making it easy to apply to the skin.'),
       ('Suppositories',
        'The active part of the medicine is combined with another substance and pressed into a ''bullet shape'' so it can be inserted into the bottom. Suppositories mustn''t be swallowed.'),
       ('Drops',
        'These are often used where the active part of the medicine works best if it reaches the affected area directly. They tend to be used for eye, ear or nose.'),
       ('Inhalers',
        'The active part of the medicine is released under pressure directly into the lungs. Young children may need to use a ''spacer'' device to take the medicine properly. Inhalers can be difficult to use at first so your pharmacist will show you how to use them.'),
       ('Injections',
        'There are different types of injection, in how and where they''re injected. Subcutaneous or SC injections are given just under the surface of the skin. Intramuscular or IM injections are given into a muscle. Intrathecal injections are given into the fluid around the spinal cord. Intravenous or IV injections are given into a vein. Some injections can be given at home but most are given at your doctor''s surgery or in hospital.'),
       ('Implants or patches',
        'These medicines are absorbed through the skin, such as nicotine patches for help in giving up smoking, or contraceptive implants.'),
       ('Tablets you don''t swallow (known as buccal or sublingual tablets or liquids)',
        'These look like normal tablets or liquids, but you don''t swallow them. Buccal medicines are held in the cheek so the mouth lining absorbs the active ingredient. Sublingual medicines work in the same way but are put underneath the tongue. Buccal and sublingual medicines tend only to be given in very specific circumstances.');

INSERT INTO side_effects (name)
VALUES ('Constipation'),
       ('Skin rash or dermatitis'),
       ('Diarrhea'),
       ('Dizziness'),
       ('Drowsiness'),
       ('Dry mouth'),
       ('Headache'),
       ('Insomnia'),
       ('Nausea'),
       ('Vomiting'),
       ('Suicidal thoughts'),
       ('Abnormal heart rhythms'),
       ('Internal bleeding'),
       ('Fever'),
       ('A general feeling of being unwell, known as malaise'),
       ('Skin reactions or pain at the vaccination site'),
       ('Death'),
       ('Device breakage or failure'),
       ('Device migration'),
       ('Infection'),
       ('Organ damage'),
       ('Stomach upset'),
       ('Hives'),
       ('Weight gain'),
       ('Yeast infection'),
       ('Nervousness'),
       ('Weakness'),
       ('Nose bleed'),
       ('Nasal irritation'),
       ('Heartburn'),
       ('Chest pain'),
       ('Loss of apetite'),
       ('Leg pain'),
       ('Constipation'),
       ('Bloating'),
       ('Tinnitus'),
       ('Numbness'),
       ('Swelling'),
       ('Minor infection'),
       ('Mild allergic or other reaction'),
       ('Bruising around the surgical site'),
       ('Redness'),
       ('Heart issues (palpitations, irregular heartbeats)'),
       ('Perforation');

INSERT INTO countries (name, two_letters_name)
VALUES ('Kosovo', 'KS'),
       ('Albania', 'KS'),
       ('Slovenia', 'SL'),
       ('Germany', 'DE'),
       ('Croatia', 'CR');


-- #############################
-- TODO: DUMMY DATA
-- #############################

INSERT INTO drugs (name, classification_id, category_id)
VALUES ('Ampicillin', 4, 41),
       ('Amoxicillin', 1, 41),
       ('Vitamin C', 4, 40);

select * from drugs;

INSERT INTO products (barcode, price_per_unit, image_path, drug_id, producer_country_id, brand_name, description,
                      warning, prescription, type_id, is_recommended)
VALUES (777777, 3, '8442f1a3-b6a5-4b3a-9fa0-5ed0bec298f3.jpg', 1, 3, 'Amcill', 'Ampicillin is a penicillin antibiotic that is used to treat or prevent many different types of infections such as bladder infections, pneumonia, gonorrhea, meningitis, or infections of the stomach or intestines.

Ampicillin may also be used for purposes not listed in this medication guide.',
        'Follow all directions on your medicine label and package. Tell each of your healthcare providers about all your medical conditions, allergies, and all medicines you use.', 'Follow all directions on your prescription label and read all medication guides or instruction sheets. Use the medicine exactly as directed.

Take this medicine with a full glass of water.

Shake the oral suspension (liquid) before you measure a dose. Use the dosing syringe provided, or use a medicine dose-measuring device (not a kitchen spoon).

Take ampicillin on an empty stomach, at least 30 minutes before or 2 hours after a meal.

Do not share this medicine with another person, even if they have the same symptoms you have.

If you are being treated for gonorrhea, your doctor may also have you tested for syphilis, another sexually transmitted disease.

If you use this medicine long-term, your kidney function, liver function, and blood cells may need to be checked.

Use this medicine for the full prescribed length of time, even if your symptoms quickly improve. Skipping doses can increase your risk of infection that is resistant to medication. Ampicillin will not treat a viral infection such as the flu or a common cold.

Very severe infections may need to be treated for several weeks.

Ampicillin can cause unusual results with certain medical tests. Tell any doctor who treats you that you are using ampicillin.

Store at room temperature away from moisture and heat. Keep the bottle tightly closed when not in use.', 3, true),

       (333333, 9, 'fb5fe2fc-35cd-4dbf-bb47-88b5ffa7fb8d.jpeg', 2, 4, 'Wymox', 'Amoxicillin is used to treat a wide variety of bacterial infections. This medication is a penicillin-type antibiotic. It works by stopping the growth of bacteria.

This antibiotic treats only bacterial infections. It will not work for viral infections (such as common cold, flu). Using any antibiotic when it is not needed can cause it to not work for future infections.

Amoxicillin is also used with other medications to treat stomach/intestinal ulcers caused by the bacteria H. pylori and to prevent the ulcers from returning.', 'Do not use this medication if you are allergic to amoxicillin or to any other penicillin antibiotic, such as ampicillin (Omnipen, Principen), dicloxacillin (Dycill, Dynapen), oxacillin (Bactocill), penicillin (Beepen-VK, Ledercillin VK, Pen-V, Pen-Vee K, Pfizerpen, V-Cillin K, Veetids), and others.

Before using amoxicillin, tell your doctor if you are allergic to cephalosporins such as Omnicef, Cefzil, Ceftin, Keflex, and others. Also tell your doctor if you have asthma, liver or kidney disease, a bleeding or blood clotting disorder, mononucleosis (also called "mono"), or any type of allergy.

Amoxicillin can make birth control pills less effective. Ask your doctor about using a non-hormone method of birth control (such as a condom, diaphragm, spermicide) to prevent pregnancy while taking this medicine. Take this medication for the full prescribed length of time. Your symptoms may improve before the infection is completely cleared. Amoxicillin will not treat a viral infection such as the common cold or flu. Do not share this medication with another person, even if they have the same symptoms you have.

Antibiotic medicines can cause diarrhea. This may happen while you are taking amoxicillin, or within a few months after you stop taking it. This may be a sign of a new infection. If you have diarrhea that is watery or bloody, stop taking this medicine and call your doctor. Do not use anti-diarrhea medicine unless your doctor tells you to.', 'Take amoxicillin exactly as prescribed by your doctor. Follow all directions on your prescription label and read all medication guides or instruction sheets.

Take amoxicillin at the same time each day.

Some forms of amoxicillin may be taken with or without food. Check your medicine label to see if you should take your mediicne with food or not.

Shake the oral suspension (liquid) before you measure a dose.

Measure liquid medicine with the dosing syringe provided, or use a medicine dose-measuring device (not a kitchen spoon). You may mix the liquid with water, milk, baby formula, fruit juice, or ginger ale. Drink all of the mixture right away. Do not save for later use.

You must chew the chewable tablet before you swallow it.

Swallow the regular tablet whole and do not crush, chew, or break it.

You will need frequent medical tests.

If you are taking amoxicillin with clarithromycin and/or lansoprazole to treat stomach ulcer, use all of your medications as directed. Read the medication guide or patient instructions provided with each medication. Do not change your doses or medication schedule without your doctor''s advice.

Use this medicine for the full prescribed length of time, even if your symptoms quickly improve. Skipping doses can increase your risk of infection that is resistant to medication. Amoxicillin will not treat a viral infection such as the flu or a common cold.

Do not share this medicine with another person, even if they have the same symptoms you have.

This medicine can affect the results of certain medical tests. Tell any doctor who treats you that you are using this medicine .

Store at room temperature away from moisture, heat, and light.

You may store liquid amoxicillin in a refrigerator but do not allow it to freeze. Throw away any liquid mediicne that is not used within 14 days after it was mixed at the pharmacy.',
        3, false),

       (666666, 5, '1b73899b-a842-4445-a7ff-7ba27079889a.jpeg', 2, 4, 'Moxatag', 'Amoxicillin is a penicillin antibiotic that fights bacteria.

Amoxicillin is used to treat many different types of infection caused by bacteria, such as tonsillitis, bronchitis, pneumonia, and infections of the ear, nose, throat, skin, or urinary tract.

Amoxicillin is also sometimes used together with another antibiotic called clarithromycin (Biaxin) to treat stomach ulcers caused by Helicobacter pylori infection. This combination is sometimes used with a stomach acid reducer called lansoprazole (Prevacid).', 'Do not use this medication if you are allergic to amoxicillin or to any other penicillin antibiotic, such as ampicillin (Omnipen, Principen), dicloxacillin (Dycill, Dynapen), oxacillin (Bactocill), penicillin (Beepen-VK, Ledercillin VK, Pen-V, Pen-Vee K, Pfizerpen, V-Cillin K, Veetids), and others.

Before using amoxicillin, tell your doctor if you are allergic to cephalosporins such as Omnicef, Cefzil, Ceftin, Keflex, and others. Also tell your doctor if you have asthma, liver or kidney disease, a bleeding or blood clotting disorder, mononucleosis (also called "mono"), or any type of allergy.

Amoxicillin can make birth control pills less effective. Ask your doctor about using a non-hormone method of birth control (such as a condom, diaphragm, spermicide) to prevent pregnancy while taking this medicine. Take this medication for the full prescribed length of time. Your symptoms may improve before the infection is completely cleared. Amoxicillin will not treat a viral infection such as the common cold or flu. Do not share this medication with another person, even if they have the same symptoms you have.

Antibiotic medicines can cause diarrhea. This may happen while you are taking amoxicillin, or within a few months after you stop taking it. This may be a sign of a new infection. If you have diarrhea that is watery or bloody, stop taking this medicine and call your doctor. Do not use anti-diarrhea medicine unless your doctor tells you to.', 'Take amoxicillin exactly as prescribed by your doctor. Follow all directions on your prescription label and read all medication guides or instruction sheets.

Take amoxicillin at the same time each day.

Some forms of amoxicillin may be taken with or without food. Check your medicine label to see if you should take your mediicne with food or not.

Shake the oral suspension (liquid) before you measure a dose.

Measure liquid medicine with the dosing syringe provided, or use a medicine dose-measuring device (not a kitchen spoon). You may mix the liquid with water, milk, baby formula, fruit juice, or ginger ale. Drink all of the mixture right away. Do not save for later use.

You must chew the chewable tablet before you swallow it.

Swallow the regular tablet whole and do not crush, chew, or break it.

You will need frequent medical tests.

If you are taking amoxicillin with clarithromycin and/or lansoprazole to treat stomach ulcer, use all of your medications as directed. Read the medication guide or patient instructions provided with each medication. Do not change your doses or medication schedule without your doctor''s advice.

Use this medicine for the full prescribed length of time, even if your symptoms quickly improve. Skipping doses can increase your risk of infection that is resistant to medication. Amoxicillin will not treat a viral infection such as the flu or a common cold.

Do not share this medicine with another person, even if they have the same symptoms you have.

This medicine can affect the results of certain medical tests. Tell any doctor who treats you that you are using this medicine .

Store at room temperature away from moisture, heat, and light.

You may store liquid amoxicillin in a refrigerator but do not allow it to freeze. Throw away any liquid mediicne that is not used within 14 days after it was mixed at the pharmacy.',
        3, true),

       (111111, 0.5, '32422f92-da01-4912-9ba8-e550cce8fba1.jpg', 3, 5, 'VitaminHaus - VÖOST', 'Ascorbic acid (vitamin C) occurs naturally in foods such as citrus fruit, tomatoes, potatoes, and leafy vegetables. Vitamin C is important for bones and connective tissues, muscles, and blood vessels. Vitamin C also helps the body absorb iron, which is needed for red blood cell production.

Ascorbic acid is used to treat and prevent vitamin C deficiency.

Ascorbic acid may also be used for purposes not listed in this medication guide.',
        'Follow all directions on your medicine label and package. Tell each of your healthcare providers about all your medical conditions, allergies, and all medicines you use.', 'Use exactly as directed on the label, or as prescribed by your doctor. Do not use in larger or smaller amounts or for longer than recommended.

The recommended dietary allowance of vitamin C (ascorbic acid) increases with age. Follow your healthcare provider''s instructions. You may also consult the Office of Dietary Supplements of the National Institutes of Health, or the U.S. Department of Agriculture (USDA) Nutrient Database (formerly "Recommended Daily Allowances") listings for more information.

Drink plenty of liquids while you are taking ascorbic acid.

The chewable tablet must be chewed before you swallow it.

Ascorbic acid gum may be chewed as long as desired and then thrown away.

Do not crush, chew, or break an extended-release tablet. Swallow it whole.

Measure liquid medicine with a special dose-measuring spoon or medicine cup. If you do not have a dose-measuring device, ask your pharmacist for one.

Keep the orally disintegrating tablet in the package until you are ready to take it. Use dry hands to remove the tablet and place it in your mouth. Do not swallow the tablet whole. Allow it to dissolve in your mouth without chewing. Swallow several times as the tablet dissolves.

Store ascorbic acid at room temperature away from moisture and heat.

Do not stop using ascorbic acid suddenly after long-term use at high doses, or you could have "conditional" vitamin C deficiency. Symptoms include bleeding gums, feeling very tired, and red or blue pinpoint spots around your hair follicles. Follow your doctor''s instructions about tapering your dose. Conditional vitamin C deficiency can be difficult to correct without medical supervision.',
        3, true);