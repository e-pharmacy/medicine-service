CREATE TABLE categories
(
    id          BIGSERIAL PRIMARY KEY NOT NULL,
    NAME        VARCHAR(500)          NOT NULL,
    description VARCHAR(5000)         NOT NULL
);

CREATE TABLE classifications
(
    id   BIGSERIAL PRIMARY KEY NOT NULL,
    NAME VARCHAR(500)          NOT NULL
);

CREATE TABLE types
(
    id          BIGSERIAL PRIMARY KEY NOT NULL,
    NAME        VARCHAR(500)          NOT NULL,
    description VARCHAR(5000)         NOT NULL
);

CREATE TABLE drugs
(
    id                BIGSERIAL PRIMARY KEY NOT NULL,
    name              VARCHAR(500)          NOT NULL,
    classification_id BIGINT REFERENCES classifications (id),
    category_id       BIGINT REFERENCES categories (id)
);

CREATE TABLE side_effects
(
    id   BIGSERIAL PRIMARY KEY NOT NULL,
    NAME VARCHAR(500)          NOT NULL
);

CREATE TABLE drugs_side_effects
(
    id             BIGSERIAL PRIMARY KEY NOT NULL,
    drug_id        BIGINT REFERENCES drugs (id),
    side_effect_id BIGINT REFERENCES side_effects (id)
);

CREATE TABLE countries
(
    id               BIGSERIAL PRIMARY KEY NOT NULL,
    name             VARCHAR(100)          NOT NULL,
    two_letters_name VARCHAR(3)            NOT NULL
);

create table products
(
    id                  BIGSERIAL PRIMARY KEY NOT NULL,
    barcode             integer               NOT NULL,
    price_per_unit      DECIMAL               NOT NULL,
    image_path          VARCHAR(1000)         NOT NULL,
    drug_id             BIGINT REFERENCES drugs (id),
    producer_country_id BIGINT REFERENCES countries (id),
    brand_name          VARCHAR(1000)         NOT NULL,
    description         VARCHAR(5000)         NOT NULL,
    warning             VARCHAR(5000),
    prescription        VARCHAR(5000),
    type_id             BIGINT REFERENCES types (id),
    is_recommended      BOOLEAN               NOT NULL DEFAULT true,
    created_date        TIMESTAMP DEFAULT current_timestamp
);