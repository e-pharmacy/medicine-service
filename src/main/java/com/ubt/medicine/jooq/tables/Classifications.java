/*
 * This file is generated by jOOQ.
 */
package com.ubt.medicine.jooq.tables;


import com.ubt.medicine.jooq.Keys;
import com.ubt.medicine.jooq.Public;
import com.ubt.medicine.jooq.tables.records.ClassificationsRecord;

import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row2;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Classifications extends TableImpl<ClassificationsRecord> {

    private static final long serialVersionUID = -605586875;

    /**
     * The reference instance of <code>public.classifications</code>
     */
    public static final Classifications CLASSIFICATIONS = new Classifications();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ClassificationsRecord> getRecordType() {
        return ClassificationsRecord.class;
    }

    /**
     * The column <code>public.classifications.id</code>.
     */
    public final TableField<ClassificationsRecord, Long> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('classifications_id_seq'::regclass)", org.jooq.impl.SQLDataType.BIGINT)), this, "");

    /**
     * The column <code>public.classifications.name</code>.
     */
    public final TableField<ClassificationsRecord, String> NAME = createField(DSL.name("name"), org.jooq.impl.SQLDataType.VARCHAR(500).nullable(false), this, "");

    /**
     * Create a <code>public.classifications</code> table reference
     */
    public Classifications() {
        this(DSL.name("classifications"), null);
    }

    /**
     * Create an aliased <code>public.classifications</code> table reference
     */
    public Classifications(String alias) {
        this(DSL.name(alias), CLASSIFICATIONS);
    }

    /**
     * Create an aliased <code>public.classifications</code> table reference
     */
    public Classifications(Name alias) {
        this(alias, CLASSIFICATIONS);
    }

    private Classifications(Name alias, Table<ClassificationsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Classifications(Name alias, Table<ClassificationsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    public <O extends Record> Classifications(Table<O> child, ForeignKey<O, ClassificationsRecord> key) {
        super(child, key, CLASSIFICATIONS);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public Identity<ClassificationsRecord, Long> getIdentity() {
        return Keys.IDENTITY_CLASSIFICATIONS;
    }

    @Override
    public UniqueKey<ClassificationsRecord> getPrimaryKey() {
        return Keys.CLASSIFICATIONS_PKEY;
    }

    @Override
    public List<UniqueKey<ClassificationsRecord>> getKeys() {
        return Arrays.<UniqueKey<ClassificationsRecord>>asList(Keys.CLASSIFICATIONS_PKEY);
    }

    @Override
    public Classifications as(String alias) {
        return new Classifications(DSL.name(alias), this);
    }

    @Override
    public Classifications as(Name alias) {
        return new Classifications(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Classifications rename(String name) {
        return new Classifications(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Classifications rename(Name name) {
        return new Classifications(name, null);
    }

    // -------------------------------------------------------------------------
    // Row2 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row2<Long, String> fieldsRow() {
        return (Row2) super.fieldsRow();
    }
}
