package com.ubt.medicine.staticdata.sideeffects;

import com.ubt.medicine.staticdata.sideeffects.filters.SideEffectFilter;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SideEffectsRepository {
  Page<SideEffectModel> getSideEffects(SideEffectFilter sideEffectFilter, Pageable pageable);

  SideEffectModel getSideEffectById(long sideEffectId);

  SideEffectModel createSideEffect(SideEffectModel sideEffectModel);

  SideEffectModel updateSideEffect(long sideEffectId, SideEffectModel sideEffectModel);

  SideEffectModel deleteSideEffect(long sideEffectId);
}
