package com.ubt.medicine.staticdata.sideeffects;

import com.ubt.medicine.staticdata.sideeffects.filters.SideEffectFilter;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface SideEffectsService {
  Page<SideEffectModel> getSideEffects(SideEffectFilter sideEffectFilter, Pageable pageable);

  SideEffectModel getSideEffectById(long sideEffectId);

  SideEffectModel createSideEffect(@Valid SideEffectModel sideEffectModel);

  SideEffectModel updateSideEffect(long sideEffectId, @Valid SideEffectModel sideEffectModel);

  SideEffectModel deleteSideEffect(long sideEffectId);
}
