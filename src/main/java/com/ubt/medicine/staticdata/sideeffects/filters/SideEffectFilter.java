package com.ubt.medicine.staticdata.sideeffects.filters;

import com.ubt.medicine.commons.filters.BaseFilter;
import com.ubt.medicine.commons.validation.ValidationService;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.DRUGS_SIDE_EFFECTS;
import static com.ubt.medicine.jooq.Tables.SIDE_EFFECTS;

public class SideEffectFilter extends BaseFilter {
  private String name;
  private List<Long> drugsIds;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Long> getDrugsIds() {
    return drugsIds;
  }

  public void setDrugsIds(List<Long> drugsIds) {
    this.drugsIds = drugsIds;
  }

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (ValidationService.notEmpty(getIds())) {
      condition = condition.and(SIDE_EFFECTS.ID.in(getIds()));
    }
    if (ValidationService.notBlank(getName())) {
      condition = condition.and(SIDE_EFFECTS.NAME.containsIgnoreCase(getName()));
    }
    if (ValidationService.notEmpty(getDrugsIds())) {
      condition = condition.and(DRUGS_SIDE_EFFECTS.DRUG_ID.in(getDrugsIds()));
    }

    return condition;
  }
}
