package com.ubt.medicine.staticdata.sideeffects;

import com.ubt.medicine.staticdata.sideeffects.filters.SideEffectFilter;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SideEffectsServiceImpl implements SideEffectsService {
  private final SideEffectsRepository categoriesRepository;

  @Autowired
  public SideEffectsServiceImpl(SideEffectsRepository categoriesRepository) {
    this.categoriesRepository = categoriesRepository;
  }


  @Override
  public Page<SideEffectModel> getSideEffects(SideEffectFilter sideEffectFilter, Pageable pageable) {
    return categoriesRepository.getSideEffects(sideEffectFilter, pageable);
  }

  @Override
  public SideEffectModel getSideEffectById(long sideEffectId) {
    return categoriesRepository.getSideEffectById(sideEffectId);
  }

  @Override
  public SideEffectModel createSideEffect(SideEffectModel sideEffectModel) {
    return categoriesRepository.createSideEffect(sideEffectModel);
  }

  @Override
  public SideEffectModel updateSideEffect(long sideEffectId, SideEffectModel sideEffectModel) {
    return categoriesRepository.updateSideEffect(sideEffectId, sideEffectModel);
  }

  @Override
  public SideEffectModel deleteSideEffect(long sideEffectId) {
    return categoriesRepository.deleteSideEffect(sideEffectId);
  }
}
