package com.ubt.medicine.staticdata.sideeffects;

import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.jooq.tables.records.SideEffectsRecord;
import com.ubt.medicine.staticdata.sideeffects.filters.SideEffectFilter;
import com.ubt.medicine.staticdata.sideeffects.jooqmappers.SideEffectJooqMapper;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.DRUGS_SIDE_EFFECTS;
import static com.ubt.medicine.jooq.Tables.SIDE_EFFECTS;

@Repository
public class SideEffectsRepositoryImpl implements SideEffectsRepository {
  private final DSLContext create;

  @Autowired
  public SideEffectsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public Page<SideEffectModel> getSideEffects(SideEffectFilter sideEffectFilter, Pageable pageable) {
    PageableModel customPageable = PageableUtils.createPageableModel(pageable);

    Condition condition = sideEffectFilter.createFilterCondition();

    int totalElements = create.selectCount().from(SIDE_EFFECTS)
            .leftOuterJoin(DRUGS_SIDE_EFFECTS).onKey()
            .where(condition)
            .fetchOneInto(Integer.class);

    List<SideEffectModel> sideEffectModels = SideEffectJooqMapper.map(
            create.select().from(SIDE_EFFECTS)
                    .leftOuterJoin(DRUGS_SIDE_EFFECTS).onKey()
                    .where(condition)
                    .orderBy(customPageable.getOrders())
                    .offset(customPageable.getOffset())
                    .limit(customPageable.getLimit())
                    .fetchInto(SideEffectsRecord.class)
    );

    return PageableUtils.newPage(sideEffectModels, customPageable, totalElements);
  }

  @Override
  public SideEffectModel getSideEffectById(long sideEffectId) {
    return SideEffectJooqMapper.map(
            create.selectFrom(SIDE_EFFECTS)
                    .where(SIDE_EFFECTS.ID.eq(sideEffectId))
                    .fetchOne()
    );
  }

  @Override
  public SideEffectModel createSideEffect(SideEffectModel sideEffectModel) {
    SideEffectsRecord categoriesRecord = SideEffectJooqMapper.unmap(sideEffectModel);

    return SideEffectJooqMapper.map(
            create.insertInto(SIDE_EFFECTS)
                    .set(categoriesRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public SideEffectModel updateSideEffect(long sideEffectId, SideEffectModel sideEffectModel) {
    SideEffectsRecord categoriesRecord = SideEffectJooqMapper.unmap(sideEffectModel);

    return SideEffectJooqMapper.map(
            create.update(SIDE_EFFECTS)
                    .set(categoriesRecord)
                    .where(SIDE_EFFECTS.ID.eq(sideEffectId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public SideEffectModel deleteSideEffect(long sideEffectId) {
    return SideEffectJooqMapper.map(
            create.delete(SIDE_EFFECTS)
                    .where(SIDE_EFFECTS.ID.eq(sideEffectId))
                    .returning()
                    .fetchOne()
    );
  }
}
