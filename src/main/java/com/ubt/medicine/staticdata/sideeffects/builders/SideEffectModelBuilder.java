package com.ubt.medicine.staticdata.sideeffects.builders;

import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;

public final class SideEffectModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private String name;

  private SideEffectModelBuilder() {
  }

  public static SideEffectModelBuilder aSideEffectModel() {
    return new SideEffectModelBuilder();
  }

  public SideEffectModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public SideEffectModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public SideEffectModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public SideEffectModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public SideEffectModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public SideEffectModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public SideEffectModel build() {
    SideEffectModel sideEffectModel = new SideEffectModel();
    sideEffectModel.setId(id);
    sideEffectModel.setCreatedDate(createdDate);
    sideEffectModel.setUpdatedDate(updatedDate);
    sideEffectModel.setCreatedBy(createdBy);
    sideEffectModel.setUpdatedBy(updatedBy);
    sideEffectModel.setName(name);
    return sideEffectModel;
  }
}
