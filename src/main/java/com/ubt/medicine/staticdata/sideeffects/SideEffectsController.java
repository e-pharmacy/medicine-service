package com.ubt.medicine.staticdata.sideeffects;

import com.ubt.medicine.staticdata.sideeffects.filters.SideEffectFilter;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/side-effects")
public class SideEffectsController {
  private final SideEffectsService sideEffectsService;

  @Autowired
  public SideEffectsController(SideEffectsService sideEffectsService) {
    this.sideEffectsService = sideEffectsService;
  }

  @GetMapping("")
  public Page<SideEffectModel> getSideEffects(SideEffectFilter sideEffectFilter, Pageable pageable) {
    return sideEffectsService.getSideEffects(sideEffectFilter, pageable);
  }

  @GetMapping("/{sideEffectId}")
  public SideEffectModel getSideEffectById(@PathVariable long sideEffectId) {
    return sideEffectsService.getSideEffectById(sideEffectId);
  }

  @PostMapping("")
  public SideEffectModel createSideEffect(@RequestBody SideEffectModel sideEffectModel) {
    return sideEffectsService.createSideEffect(sideEffectModel);
  }

  @PutMapping("/{sideEffectId}")
  public SideEffectModel updateSideEffect(@PathVariable long sideEffectId, @RequestBody SideEffectModel sideEffectModel) {
    return sideEffectsService.updateSideEffect(sideEffectId, sideEffectModel);
  }

  @DeleteMapping("/{sideEffectId}")
  public SideEffectModel deleteSideEffect(@PathVariable long sideEffectId) {
    return sideEffectsService.deleteSideEffect(sideEffectId);
  }
}
