package com.ubt.medicine.staticdata.sideeffects.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.jooq.tables.records.SideEffectsRecord;
import com.ubt.medicine.staticdata.sideeffects.builders.SideEffectModelBuilder;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SideEffectJooqMapper {
  public static SideEffectModel map(SideEffectsRecord record) {
    SideEffectModel model = null;
    if (record != null) {
      model = SideEffectModelBuilder.aSideEffectModel()
              .withId(record.getId())
              .withName(record.getName())
              .build();
    }
    return model;
  }

  public static SideEffectsRecord unmap(SideEffectModel model) {
    SideEffectsRecord record = new SideEffectsRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getName())) {
      record.setName(model.getName());
    }

    return record;
  }

  public static List<SideEffectModel> map(List<SideEffectsRecord> records) {
    List<SideEffectModel> models = new ArrayList<>();

    for (SideEffectsRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
