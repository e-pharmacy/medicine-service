package com.ubt.medicine.staticdata.sideeffects.models;

import com.ubt.medicine.commons.models.BoBaseModel;

import javax.validation.constraints.NotBlank;

public class SideEffectModel extends BoBaseModel {
  @NotBlank(message = "{sideEffect.name.notBlank}")
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
