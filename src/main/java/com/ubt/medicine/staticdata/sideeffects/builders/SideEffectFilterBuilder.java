package com.ubt.medicine.staticdata.sideeffects.builders;

import com.ubt.medicine.staticdata.sideeffects.filters.SideEffectFilter;

import java.util.List;

public final class SideEffectFilterBuilder {
  private List<Long> ids;
  private String name;
  private List<Long> drugsIds;

  private SideEffectFilterBuilder() {
  }

  public static SideEffectFilterBuilder aSideEffectFilter() {
    return new SideEffectFilterBuilder();
  }

  public SideEffectFilterBuilder withIds(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public SideEffectFilterBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public SideEffectFilterBuilder withDrugsIds(List<Long> drugsIds) {
    this.drugsIds = drugsIds;
    return this;
  }

  public SideEffectFilter build() {
    SideEffectFilter sideEffectFilter = new SideEffectFilter();
    sideEffectFilter.setIds(ids);
    sideEffectFilter.setName(name);
    sideEffectFilter.setDrugsIds(drugsIds);
    return sideEffectFilter;
  }
}
