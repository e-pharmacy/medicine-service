package com.ubt.medicine.staticdata.countries;

import com.ubt.medicine.staticdata.countries.filters.CountryFilter;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface CountriesService {
  Page<CountryModel> getCountries(CountryFilter countryFilter, Pageable pageable);

  CountryModel getCountryById(long countryId);

  CountryModel createCountry(@Valid CountryModel countryModel);

  CountryModel updateCountry(long countryId, @Valid CountryModel countryModel);

  CountryModel deleteCountry(long countryId);
}
