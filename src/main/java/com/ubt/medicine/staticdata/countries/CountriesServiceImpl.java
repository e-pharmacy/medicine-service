package com.ubt.medicine.staticdata.countries;

import com.ubt.medicine.staticdata.countries.filters.CountryFilter;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CountriesServiceImpl implements CountriesService {
  private final CountriesRepository countriesRepository;

  @Autowired
  public CountriesServiceImpl(CountriesRepository countriesRepository) {
    this.countriesRepository = countriesRepository;
  }


  @Override
  public Page<CountryModel> getCountries(CountryFilter countryFilter, Pageable pageable) {
    return countriesRepository.getCountries(countryFilter, pageable);
  }

  @Override
  public CountryModel getCountryById(long countryId) {
    return countriesRepository.getCountryById(countryId);
  }

  @Override
  public CountryModel createCountry(CountryModel countryModel) {
    return countriesRepository.createCountry(countryModel);
  }

  @Override
  public CountryModel updateCountry(long countryId, CountryModel countryModel) {
    return countriesRepository.updateCountry(countryId, countryModel);
  }

  @Override
  public CountryModel deleteCountry(long countryId) {
    return countriesRepository.deleteCountry(countryId);
  }
}
