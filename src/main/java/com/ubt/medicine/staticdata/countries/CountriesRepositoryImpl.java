package com.ubt.medicine.staticdata.countries;

import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.jooq.tables.records.CountriesRecord;
import com.ubt.medicine.staticdata.countries.filters.CountryFilter;
import com.ubt.medicine.staticdata.countries.jooqmappers.CountryJooqMapper;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.COUNTRIES;

@Repository
public class CountriesRepositoryImpl implements CountriesRepository {
  private final DSLContext create;

  @Autowired
  public CountriesRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public Page<CountryModel> getCountries(CountryFilter countryFilter, Pageable pageable) {
    PageableModel customPageable = PageableUtils.createPageableModel(pageable);

    Condition condition = countryFilter.createFilterCondition();

    int totalElements = create.selectCount().from(COUNTRIES)
            .where(condition)
            .fetchOneInto(Integer.class);

    List<CountryModel> countryModels = CountryJooqMapper.map(
            create.selectFrom(COUNTRIES)
                    .where(condition)
                    .orderBy(customPageable.getOrders())
                    .offset(customPageable.getOffset())
                    .limit(customPageable.getLimit())
                    .fetchInto(CountriesRecord.class)
    );

    return PageableUtils.newPage(countryModels, customPageable, totalElements);
  }

  @Override
  public CountryModel getCountryById(long countryId) {
    return CountryJooqMapper.map(
            create.selectFrom(COUNTRIES)
                    .where(COUNTRIES.ID.eq(countryId))
                    .fetchOne()
    );
  }

  @Override
  public CountryModel createCountry(CountryModel countryModel) {
    CountriesRecord countriesRecord = CountryJooqMapper.unmap(countryModel);

    return CountryJooqMapper.map(
            create.insertInto(COUNTRIES)
                    .set(countriesRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public CountryModel updateCountry(long countryId, CountryModel countryModel) {
    CountriesRecord countriesRecord = CountryJooqMapper.unmap(countryModel);

    return CountryJooqMapper.map(
            create.update(COUNTRIES)
                    .set(countriesRecord)
                    .where(COUNTRIES.ID.eq(countryId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public CountryModel deleteCountry(long countryId) {
    return CountryJooqMapper.map(
            create.delete(COUNTRIES)
                    .where(COUNTRIES.ID.eq(countryId))
                    .returning()
                    .fetchOne()
    );
  }
}
