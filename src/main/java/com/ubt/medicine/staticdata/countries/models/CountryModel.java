package com.ubt.medicine.staticdata.countries.models;

import com.ubt.medicine.commons.models.BoBaseModel;

import javax.validation.constraints.NotBlank;

public class CountryModel extends BoBaseModel {
  @NotBlank(message = "{country.name.notBlank}")
  private String name;
  @NotBlank(message = "{country.twoLettersName.notBlank}")
  private String twoLettersName;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTwoLettersName() {
    return twoLettersName;
  }

  public void setTwoLettersName(String twoLettersName) {
    this.twoLettersName = twoLettersName;
  }
}
