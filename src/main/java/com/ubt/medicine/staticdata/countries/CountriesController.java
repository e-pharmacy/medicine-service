package com.ubt.medicine.staticdata.countries;

import com.ubt.medicine.staticdata.countries.filters.CountryFilter;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/countries")
public class CountriesController {
  private final CountriesService countriesService;

  @Autowired
  public CountriesController(CountriesService countriesService) {
    this.countriesService = countriesService;
  }

  @GetMapping("")
  public Page<CountryModel> getCountries(CountryFilter countryFilter, Pageable pageable) {
    return countriesService.getCountries(countryFilter, pageable);
  }

  @GetMapping("/{countryId}")
  public CountryModel getCountryById(@PathVariable long countryId) {
    return countriesService.getCountryById(countryId);
  }

  @PostMapping("")
  public CountryModel createCountry(@RequestBody CountryModel countryModel) {
    return countriesService.createCountry(countryModel);
  }

  @PutMapping("/{countryId}")
  public CountryModel updateCountry(@PathVariable long countryId, @RequestBody CountryModel countryModel) {
    return countriesService.updateCountry(countryId, countryModel);
  }

  @DeleteMapping("/{countryId}")
  public CountryModel deleteCountry(@PathVariable long countryId) {
    return countriesService.deleteCountry(countryId);
  }
}
