package com.ubt.medicine.staticdata.countries.builders;

import com.ubt.medicine.staticdata.countries.models.CountryModel;

public final class CountryModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private String name;
  private String twoLettersName;

  private CountryModelBuilder() {
  }

  public static CountryModelBuilder aCountryModel() {
    return new CountryModelBuilder();
  }

  public CountryModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public CountryModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public CountryModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public CountryModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public CountryModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public CountryModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public CountryModelBuilder withTwoLettersName(String twoLettersName) {
    this.twoLettersName = twoLettersName;
    return this;
  }

  public CountryModel build() {
    CountryModel countryModel = new CountryModel();
    countryModel.setId(id);
    countryModel.setCreatedDate(createdDate);
    countryModel.setUpdatedDate(updatedDate);
    countryModel.setCreatedBy(createdBy);
    countryModel.setUpdatedBy(updatedBy);
    countryModel.setName(name);
    countryModel.setTwoLettersName(twoLettersName);
    return countryModel;
  }
}
