package com.ubt.medicine.staticdata.countries.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.jooq.tables.records.CountriesRecord;
import com.ubt.medicine.staticdata.countries.builders.CountryModelBuilder;
import com.ubt.medicine.staticdata.countries.models.CountryModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CountryJooqMapper {
  public static CountryModel map(CountriesRecord record) {
    CountryModel model = null;
    if (record != null) {
      model = CountryModelBuilder.aCountryModel()
              .withId(record.getId())
              .withName(record.getName())
              .withTwoLettersName(record.getTwoLettersName())
              .build();
    }
    return model;
  }

  public static CountriesRecord unmap(CountryModel model) {
    CountriesRecord record = new CountriesRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getName())) {
      record.setName(model.getName());
    }
    if (ValidationService.notBlank(model.getTwoLettersName())) {
      record.setTwoLettersName(model.getTwoLettersName());
    }

    return record;
  }

  public static List<CountryModel> map(List<CountriesRecord> records) {
    List<CountryModel> models = new ArrayList<>();

    for (CountriesRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
