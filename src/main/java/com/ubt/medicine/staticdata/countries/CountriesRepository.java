package com.ubt.medicine.staticdata.countries;

import com.ubt.medicine.staticdata.countries.filters.CountryFilter;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CountriesRepository {
  Page<CountryModel> getCountries(CountryFilter countryFilter, Pageable pageable);

  CountryModel getCountryById(long countryId);

  CountryModel createCountry(CountryModel countryModel);

  CountryModel updateCountry(long countryId, CountryModel countryModel);

  CountryModel deleteCountry(long countryId);
}
