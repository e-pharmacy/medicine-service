package com.ubt.medicine.staticdata.countries.builders;

import com.ubt.medicine.staticdata.countries.filters.CountryFilter;

import java.util.List;

public final class CountryFilterBuilder {
  private List<Long> ids;
  private String name;

  private CountryFilterBuilder() {
  }

  public static CountryFilterBuilder aCountryFilter() {
    return new CountryFilterBuilder();
  }

  public CountryFilterBuilder withIds(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public CountryFilterBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public CountryFilter build() {
    CountryFilter countryFilter = new CountryFilter();
    countryFilter.setIds(ids);
    countryFilter.setName(name);
    return countryFilter;
  }
}
