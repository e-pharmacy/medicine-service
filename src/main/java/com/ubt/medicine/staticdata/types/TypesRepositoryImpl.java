package com.ubt.medicine.staticdata.types;

import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.jooq.tables.records.TypesRecord;
import com.ubt.medicine.staticdata.types.filters.TypeFilter;
import com.ubt.medicine.staticdata.types.jooqmappers.TypeJooqMapper;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.TYPES;

@Repository
public class TypesRepositoryImpl implements TypesRepository {
  private final DSLContext create;

  @Autowired
  public TypesRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public Page<TypeModel> getTypes(TypeFilter typeFilter, Pageable pageable) {
    PageableModel customPageable = PageableUtils.createPageableModel(pageable);

    Condition condition = typeFilter.createFilterCondition();

    int totalElements = create.selectCount().from(TYPES)
            .where(condition)
            .fetchOneInto(Integer.class);

    List<TypeModel> typeModels = TypeJooqMapper.map(
            create.selectFrom(TYPES)
                    .where(condition)
                    .orderBy(customPageable.getOrders())
                    .offset(customPageable.getOffset())
                    .limit(customPageable.getLimit())
                    .fetchInto(TypesRecord.class)
    );

    return PageableUtils.newPage(typeModels, customPageable, totalElements);
  }

  @Override
  public TypeModel getTypeById(long typeId) {
    return TypeJooqMapper.map(
            create.selectFrom(TYPES)
                    .where(TYPES.ID.eq(typeId))
                    .fetchOne()
    );
  }

  @Override
  public TypeModel createType(TypeModel typeModel) {
    TypesRecord categoriesRecord = TypeJooqMapper.unmap(typeModel);

    return TypeJooqMapper.map(
            create.insertInto(TYPES)
                    .set(categoriesRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public TypeModel updateType(long typeId, TypeModel typeModel) {
    TypesRecord categoriesRecord = TypeJooqMapper.unmap(typeModel);

    return TypeJooqMapper.map(
            create.update(TYPES)
                    .set(categoriesRecord)
                    .where(TYPES.ID.eq(typeId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public TypeModel deleteType(long typeId) {
    return TypeJooqMapper.map(
            create.delete(TYPES)
                    .where(TYPES.ID.eq(typeId))
                    .returning()
                    .fetchOne()
    );
  }
}
