package com.ubt.medicine.staticdata.types.builders;

import com.ubt.medicine.staticdata.types.models.TypeModel;

public final class TypeModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private String name;
  private String description;

  private TypeModelBuilder() {
  }

  public static TypeModelBuilder aTypeModel() {
    return new TypeModelBuilder();
  }

  public TypeModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public TypeModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public TypeModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public TypeModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public TypeModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public TypeModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public TypeModelBuilder withDescription(String description) {
    this.description = description;
    return this;
  }

  public TypeModel build() {
    TypeModel typeModel = new TypeModel();
    typeModel.setId(id);
    typeModel.setCreatedDate(createdDate);
    typeModel.setUpdatedDate(updatedDate);
    typeModel.setCreatedBy(createdBy);
    typeModel.setUpdatedBy(updatedBy);
    typeModel.setName(name);
    typeModel.setDescription(description);
    return typeModel;
  }
}
