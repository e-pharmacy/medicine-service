package com.ubt.medicine.staticdata.types.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.jooq.tables.records.TypesRecord;
import com.ubt.medicine.staticdata.types.builders.TypeModelBuilder;
import com.ubt.medicine.staticdata.types.models.TypeModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TypeJooqMapper {
  public static TypeModel map(TypesRecord record) {
    TypeModel model = null;
    if (record != null) {
      model = TypeModelBuilder.aTypeModel()
              .withId(record.getId())
              .withName(record.getName())
              .withDescription(record.getDescription())
              .build();
    }
    return model;
  }

  public static TypesRecord unmap(TypeModel model) {
    TypesRecord record = new TypesRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getName())) {
      record.setName(model.getName());
    }
    if (ValidationService.notBlank(model.getDescription())) {
      record.setDescription(model.getDescription());
    }

    return record;
  }

  public static List<TypeModel> map(List<TypesRecord> records) {
    List<TypeModel> models = new ArrayList<>();

    for (TypesRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
