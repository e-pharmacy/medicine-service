package com.ubt.medicine.staticdata.types.builders;

import com.ubt.medicine.staticdata.types.filters.TypeFilter;

import java.util.List;

public final class TypeFilterBuilder {
  private List<Long> ids;
  private String name;

  private TypeFilterBuilder() {
  }

  public static TypeFilterBuilder aTypeFilter() {
    return new TypeFilterBuilder();
  }

  public TypeFilterBuilder withIds(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public TypeFilterBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public TypeFilter build() {
    TypeFilter typeFilter = new TypeFilter();
    typeFilter.setIds(ids);
    typeFilter.setName(name);
    return typeFilter;
  }
}
