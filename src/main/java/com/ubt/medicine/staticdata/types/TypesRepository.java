package com.ubt.medicine.staticdata.types;

import com.ubt.medicine.staticdata.types.filters.TypeFilter;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TypesRepository {
  Page<TypeModel> getTypes(TypeFilter typeFilter, Pageable pageable);

  TypeModel getTypeById(long typeId);

  TypeModel createType(TypeModel typeModel);

  TypeModel updateType(long typeId, TypeModel typeModel);

  TypeModel deleteType(long typeId);
}
