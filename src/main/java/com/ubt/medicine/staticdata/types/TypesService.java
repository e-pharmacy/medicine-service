package com.ubt.medicine.staticdata.types;

import com.ubt.medicine.staticdata.types.filters.TypeFilter;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface TypesService {
  Page<TypeModel> getTypes(TypeFilter typeFilter, Pageable pageable);

  TypeModel getTypeById(long typeId);

  TypeModel createType(@Valid TypeModel typeModel);

  TypeModel updateType(long typeId, @Valid TypeModel typeModel);

  TypeModel deleteType(long typeId);
}
