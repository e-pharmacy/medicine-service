package com.ubt.medicine.staticdata.types;

import com.ubt.medicine.staticdata.types.filters.TypeFilter;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TypesServiceImpl implements TypesService {
  private final TypesRepository typesRepository;

  @Autowired
  public TypesServiceImpl(TypesRepository typesRepository) {
    this.typesRepository = typesRepository;
  }


  @Override
  public Page<TypeModel> getTypes(TypeFilter typeFilter, Pageable pageable) {
    return typesRepository.getTypes(typeFilter, pageable);
  }

  @Override
  public TypeModel getTypeById(long typeId) {
    return typesRepository.getTypeById(typeId);
  }

  @Override
  public TypeModel createType(TypeModel typeModel) {
    return typesRepository.createType(typeModel);
  }

  @Override
  public TypeModel updateType(long typeId, TypeModel typeModel) {
    return typesRepository.updateType(typeId, typeModel);
  }

  @Override
  public TypeModel deleteType(long typeId) {
    return typesRepository.deleteType(typeId);
  }
}
