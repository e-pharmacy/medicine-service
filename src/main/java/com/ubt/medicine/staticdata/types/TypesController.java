package com.ubt.medicine.staticdata.types;

import com.ubt.medicine.staticdata.types.filters.TypeFilter;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/types")
public class TypesController {
  private final TypesService typesService;

  @Autowired
  public TypesController(TypesService typesService) {
    this.typesService = typesService;
  }

  @GetMapping("")
  public Page<TypeModel> getTypes(TypeFilter typeFilter, Pageable pageable) {
    return typesService.getTypes(typeFilter, pageable);
  }

  @GetMapping("/{typeId}")
  public TypeModel getTypeById(@PathVariable long typeId) {
    return typesService.getTypeById(typeId);
  }

  @PostMapping("")
  public TypeModel createType(@RequestBody TypeModel typeModel) {
    return typesService.createType(typeModel);
  }

  @PutMapping("/{typeId}")
  public TypeModel updateType(@PathVariable long typeId, @RequestBody TypeModel typeModel) {
    return typesService.updateType(typeId, typeModel);
  }

  @DeleteMapping("/{typeId}")
  public TypeModel deleteType(@PathVariable long typeId) {
    return typesService.deleteType(typeId);
  }
}
