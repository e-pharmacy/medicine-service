package com.ubt.medicine.staticdata.types.models;

import com.ubt.medicine.commons.models.BoBaseModel;

import javax.validation.constraints.NotBlank;

public class TypeModel extends BoBaseModel {
  @NotBlank(message = "{type.name.notBlank}")
  private String name;
  @NotBlank(message = "{type.description.notBlank}")
  private String description;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
