package com.ubt.medicine.staticdata.categories;

import com.ubt.medicine.staticdata.categories.filters.CategoryFilter;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
public class CategoriesController {
  private final CategoriesService categoriesService;

  @Autowired
  public CategoriesController(CategoriesService categoriesService) {
    this.categoriesService = categoriesService;
  }

  @GetMapping("")
  public Page<CategoryModel> getCategories(CategoryFilter categoryFilter, Pageable pageable) {
    return categoriesService.getCategories(categoryFilter, pageable);
  }

  @GetMapping("/{categoryId}")
  public CategoryModel getCategoryById(@PathVariable long categoryId) {
    return categoriesService.getCategoryById(categoryId);
  }

  @PostMapping("")
  public CategoryModel createCategory(@RequestBody CategoryModel categoryModel) {
    return categoriesService.createCategory(categoryModel);
  }

  @PutMapping("/{categoryId}")
  public CategoryModel updateCategory(@PathVariable long categoryId, @RequestBody CategoryModel categoryModel) {
    return categoriesService.updateCategory(categoryId, categoryModel);
  }

  @DeleteMapping("/{categoryId}")
  public CategoryModel deleteCategory(@PathVariable long categoryId) {
    return categoriesService.deleteCategory(categoryId);
  }
}
