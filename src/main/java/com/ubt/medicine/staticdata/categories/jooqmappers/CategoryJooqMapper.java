package com.ubt.medicine.staticdata.categories.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.jooq.tables.records.CategoriesRecord;
import com.ubt.medicine.staticdata.categories.builders.CategoryModelBuilder;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CategoryJooqMapper {
  public static CategoryModel map(CategoriesRecord record) {
    CategoryModel model = null;
    if (record != null) {
      model = CategoryModelBuilder.aCategoryModel()
              .withId(record.getId())
              .withName(record.getName())
              .withDescription(record.getDescription())
              .build();
    }
    return model;
  }

  public static CategoriesRecord unmap(CategoryModel model) {
    CategoriesRecord record = new CategoriesRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getName())) {
      record.setName(model.getName());
    }
    if (ValidationService.notBlank(model.getDescription())) {
      record.setDescription(model.getDescription());
    }

    return record;
  }

  public static List<CategoryModel> map(List<CategoriesRecord> records) {
    List<CategoryModel> models = new ArrayList<>();

    for (CategoriesRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
