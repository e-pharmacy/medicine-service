package com.ubt.medicine.staticdata.categories;

import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.jooq.tables.records.CategoriesRecord;
import com.ubt.medicine.staticdata.categories.filters.CategoryFilter;
import com.ubt.medicine.staticdata.categories.jooqmappers.CategoryJooqMapper;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.CATEGORIES;

@Repository
public class CategoriesRepositoryImpl implements CategoriesRepository {
  private final DSLContext create;

  @Autowired
  public CategoriesRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public Page<CategoryModel> getCategories(CategoryFilter categoryFilter, Pageable pageable) {
    PageableModel customPageable = PageableUtils.createPageableModel(pageable);

    Condition condition = categoryFilter.createFilterCondition();

    int totalElements = create.selectCount().from(CATEGORIES)
            .where(condition)
            .fetchOneInto(Integer.class);

    List<CategoryModel> categoryModels = CategoryJooqMapper.map(
            create.selectFrom(CATEGORIES)
                    .where(condition)
                    .orderBy(customPageable.getOrders())
                    .offset(customPageable.getOffset())
                    .limit(customPageable.getLimit())
                    .fetchInto(CategoriesRecord.class)
    );

    return PageableUtils.newPage(categoryModels, customPageable, totalElements);
  }

  @Override
  public CategoryModel getCategoryById(long categoryId) {
    return CategoryJooqMapper.map(
            create.selectFrom(CATEGORIES)
                    .where(CATEGORIES.ID.eq(categoryId))
                    .fetchOne()
    );
  }

  @Override
  public CategoryModel createCategory(CategoryModel categoryModel) {
    CategoriesRecord categoriesRecord = CategoryJooqMapper.unmap(categoryModel);

    return CategoryJooqMapper.map(
            create.insertInto(CATEGORIES)
                    .set(categoriesRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public CategoryModel updateCategory(long categoryId, CategoryModel categoryModel) {
    CategoriesRecord categoriesRecord = CategoryJooqMapper.unmap(categoryModel);

    return CategoryJooqMapper.map(
            create.update(CATEGORIES)
                    .set(categoriesRecord)
                    .where(CATEGORIES.ID.eq(categoryId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public CategoryModel deleteCategory(long categoryId) {
    return CategoryJooqMapper.map(
            create.delete(CATEGORIES)
                    .where(CATEGORIES.ID.eq(categoryId))
                    .returning()
                    .fetchOne()
    );
  }
}
