package com.ubt.medicine.staticdata.categories.builders;

import com.ubt.medicine.staticdata.categories.filters.CategoryFilter;

import java.util.List;

public final class CategoryFilterBuilder {
  private List<Long> ids;
  private String name;

  private CategoryFilterBuilder() {
  }

  public static CategoryFilterBuilder aCategoryFilter() {
    return new CategoryFilterBuilder();
  }

  public CategoryFilterBuilder withIds(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public CategoryFilterBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public CategoryFilter build() {
    CategoryFilter categoryFilter = new CategoryFilter();
    categoryFilter.setIds(ids);
    categoryFilter.setName(name);
    return categoryFilter;
  }
}
