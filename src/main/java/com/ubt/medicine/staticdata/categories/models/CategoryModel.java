package com.ubt.medicine.staticdata.categories.models;

import com.ubt.medicine.commons.models.BoBaseModel;

import javax.validation.constraints.NotBlank;

public class CategoryModel extends BoBaseModel {
  @NotBlank(message = "{category.name.notBlank}")
  private String name;
  @NotBlank(message = "{category.description.notBlank}")
  private String description;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
