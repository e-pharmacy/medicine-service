package com.ubt.medicine.staticdata.categories.builders;

import com.ubt.medicine.staticdata.categories.models.CategoryModel;

public final class CategoryModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private String name;
  private String description;

  private CategoryModelBuilder() {
  }

  public static CategoryModelBuilder aCategoryModel() {
    return new CategoryModelBuilder();
  }

  public CategoryModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public CategoryModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public CategoryModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public CategoryModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public CategoryModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public CategoryModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public CategoryModelBuilder withDescription(String description) {
    this.description = description;
    return this;
  }

  public CategoryModel build() {
    CategoryModel categoryModel = new CategoryModel();
    categoryModel.setId(id);
    categoryModel.setCreatedDate(createdDate);
    categoryModel.setUpdatedDate(updatedDate);
    categoryModel.setCreatedBy(createdBy);
    categoryModel.setUpdatedBy(updatedBy);
    categoryModel.setName(name);
    categoryModel.setDescription(description);
    return categoryModel;
  }
}
