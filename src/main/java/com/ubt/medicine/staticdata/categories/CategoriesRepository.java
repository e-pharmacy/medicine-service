package com.ubt.medicine.staticdata.categories;

import com.ubt.medicine.staticdata.categories.filters.CategoryFilter;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoriesRepository {
  Page<CategoryModel> getCategories(CategoryFilter categoryFilter, Pageable pageable);

  CategoryModel getCategoryById(long categoryId);

  CategoryModel createCategory(CategoryModel categoryModel);

  CategoryModel updateCategory(long categoryId, CategoryModel categoryModel);

  CategoryModel deleteCategory(long categoryId);
}
