package com.ubt.medicine.staticdata.categories;

import com.ubt.medicine.staticdata.categories.filters.CategoryFilter;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface CategoriesService {
  Page<CategoryModel> getCategories(CategoryFilter categoryFilter, Pageable pageable);

  CategoryModel getCategoryById(long categoryId);

  CategoryModel createCategory(@Valid CategoryModel categoryModel);

  CategoryModel updateCategory(long categoryId, @Valid CategoryModel categoryModel);

  CategoryModel deleteCategory(long categoryId);
}
