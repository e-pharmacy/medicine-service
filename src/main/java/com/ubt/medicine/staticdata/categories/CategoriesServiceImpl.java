package com.ubt.medicine.staticdata.categories;

import com.ubt.medicine.staticdata.categories.filters.CategoryFilter;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategoriesServiceImpl implements CategoriesService {
  private final CategoriesRepository categoriesRepository;

  @Autowired
  public CategoriesServiceImpl(CategoriesRepository categoriesRepository) {
    this.categoriesRepository = categoriesRepository;
  }


  @Override
  public Page<CategoryModel> getCategories(CategoryFilter categoryFilter, Pageable pageable) {
    return categoriesRepository.getCategories(categoryFilter, pageable);
  }

  @Override
  public CategoryModel getCategoryById(long categoryId) {
    return categoriesRepository.getCategoryById(categoryId);
  }

  @Override
  public CategoryModel createCategory(CategoryModel categoryModel) {
    return categoriesRepository.createCategory(categoryModel);
  }

  @Override
  public CategoryModel updateCategory(long categoryId, CategoryModel categoryModel) {
    return categoriesRepository.updateCategory(categoryId, categoryModel);
  }

  @Override
  public CategoryModel deleteCategory(long categoryId) {
    return categoriesRepository.deleteCategory(categoryId);
  }
}
