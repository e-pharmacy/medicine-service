package com.ubt.medicine.staticdata.classifications;

import com.ubt.medicine.staticdata.classifications.filters.ClassificationFilter;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/classifications")
public class ClassificationsController {
  private final ClassificationsService classificationsService;

  @Autowired
  public ClassificationsController(ClassificationsService classificationsService) {
    this.classificationsService = classificationsService;
  }

  @GetMapping("")
  public Page<ClassificationModel> getClassifications(ClassificationFilter classificationFilter, Pageable pageable) {
    return classificationsService.getClassifications(classificationFilter, pageable);
  }

  @GetMapping("/{classificationId}")
  public ClassificationModel getClassificationById(@PathVariable long classificationId) {
    return classificationsService.getClassificationById(classificationId);
  }

  @PostMapping("")
  public ClassificationModel createClassification(@RequestBody ClassificationModel classificationModel) {
    return classificationsService.createClassification(classificationModel);
  }

  @PutMapping("/{classificationId}")
  public ClassificationModel updateClassification(@PathVariable long classificationId, @RequestBody ClassificationModel classificationModel) {
    return classificationsService.updateClassification(classificationId, classificationModel);
  }

  @DeleteMapping("/{classificationId}")
  public ClassificationModel deleteClassification(@PathVariable long classificationId) {
    return classificationsService.deleteClassification(classificationId);
  }
}
