package com.ubt.medicine.staticdata.classifications;

import com.ubt.medicine.staticdata.classifications.filters.ClassificationFilter;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ClassificationsServiceImpl implements ClassificationsService {
  private final ClassificationsRepository classificationsRepository;

  @Autowired
  public ClassificationsServiceImpl(ClassificationsRepository classificationsRepository) {
    this.classificationsRepository = classificationsRepository;
  }


  @Override
  public Page<ClassificationModel> getClassifications(ClassificationFilter classificationFilter, Pageable pageable) {
    return classificationsRepository.getClassifications(classificationFilter, pageable);
  }

  @Override
  public ClassificationModel getClassificationById(long classificationId) {
    return classificationsRepository.getClassificationById(classificationId);
  }

  @Override
  public ClassificationModel createClassification(ClassificationModel classificationModel) {
    return classificationsRepository.createClassification(classificationModel);
  }

  @Override
  public ClassificationModel updateClassification(long classificationId, ClassificationModel classificationModel) {
    return classificationsRepository.updateClassification(classificationId, classificationModel);
  }

  @Override
  public ClassificationModel deleteClassification(long classificationId) {
    return classificationsRepository.deleteClassification(classificationId);
  }
}
