package com.ubt.medicine.staticdata.classifications.models;

import com.ubt.medicine.commons.models.BoBaseModel;

import javax.validation.constraints.NotBlank;

public class ClassificationModel extends BoBaseModel {
  @NotBlank(message = "{classification.name.notBlank}")
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
