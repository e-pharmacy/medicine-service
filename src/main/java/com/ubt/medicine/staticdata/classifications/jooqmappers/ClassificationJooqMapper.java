package com.ubt.medicine.staticdata.classifications.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.jooq.tables.records.ClassificationsRecord;
import com.ubt.medicine.staticdata.classifications.builders.ClassificationModelBuilder;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ClassificationJooqMapper {
  public static ClassificationModel map(ClassificationsRecord record) {
    ClassificationModel model = null;
    if (record != null) {
      model = ClassificationModelBuilder.aClassificationModel()
              .withId(record.getId())
              .withName(record.getName())
              .build();
    }
    return model;
  }

  public static ClassificationsRecord unmap(ClassificationModel model) {
    ClassificationsRecord record = new ClassificationsRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getName())) {
      record.setName(model.getName());
    }

    return record;
  }

  public static List<ClassificationModel> map(List<ClassificationsRecord> records) {
    List<ClassificationModel> models = new ArrayList<>();

    for (ClassificationsRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
