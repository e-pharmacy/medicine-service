package com.ubt.medicine.staticdata.classifications;

import com.ubt.medicine.staticdata.classifications.filters.ClassificationFilter;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface ClassificationsService {
  Page<ClassificationModel> getClassifications(ClassificationFilter classificationFilter, Pageable pageable);

  ClassificationModel getClassificationById(long classificationId);

  ClassificationModel createClassification(@Valid ClassificationModel classificationModel);

  ClassificationModel updateClassification(long classificationId, @Valid ClassificationModel classificationModel);

  ClassificationModel deleteClassification(long classificationId);
}
