package com.ubt.medicine.staticdata.classifications.builders;

import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;

public final class ClassificationModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private String name;

  private ClassificationModelBuilder() {
  }

  public static ClassificationModelBuilder aClassificationModel() {
    return new ClassificationModelBuilder();
  }

  public ClassificationModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public ClassificationModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public ClassificationModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public ClassificationModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public ClassificationModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public ClassificationModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public ClassificationModel build() {
    ClassificationModel classificationModel = new ClassificationModel();
    classificationModel.setId(id);
    classificationModel.setCreatedDate(createdDate);
    classificationModel.setUpdatedDate(updatedDate);
    classificationModel.setCreatedBy(createdBy);
    classificationModel.setUpdatedBy(updatedBy);
    classificationModel.setName(name);
    return classificationModel;
  }
}
