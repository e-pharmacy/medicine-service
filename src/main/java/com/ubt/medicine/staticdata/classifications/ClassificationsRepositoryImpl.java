package com.ubt.medicine.staticdata.classifications;

import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.jooq.tables.records.ClassificationsRecord;
import com.ubt.medicine.staticdata.classifications.filters.ClassificationFilter;
import com.ubt.medicine.staticdata.classifications.jooqmappers.ClassificationJooqMapper;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.CLASSIFICATIONS;

@Repository
public class ClassificationsRepositoryImpl implements ClassificationsRepository {
  private final DSLContext create;

  @Autowired
  public ClassificationsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public Page<ClassificationModel> getClassifications(ClassificationFilter classificationFilter, Pageable pageable) {
    PageableModel customPageable = PageableUtils.createPageableModel(pageable);

    Condition condition = classificationFilter.createFilterCondition();

    int totalElements = create.selectCount().from(CLASSIFICATIONS)
            .where(condition)
            .fetchOneInto(Integer.class);

    List<ClassificationModel> classificationModels = ClassificationJooqMapper.map(
            create.selectFrom(CLASSIFICATIONS)
                    .where(condition)
                    .orderBy(customPageable.getOrders())
                    .offset(customPageable.getOffset())
                    .limit(customPageable.getLimit())
                    .fetchInto(ClassificationsRecord.class)
    );

    return PageableUtils.newPage(classificationModels, customPageable, totalElements);
  }

  @Override
  public ClassificationModel getClassificationById(long classificationId) {
    return ClassificationJooqMapper.map(
            create.selectFrom(CLASSIFICATIONS)
                    .where(CLASSIFICATIONS.ID.eq(classificationId))
                    .fetchOne()
    );
  }

  @Override
  public ClassificationModel createClassification(ClassificationModel classificationModel) {
    ClassificationsRecord classificationsRecord = ClassificationJooqMapper.unmap(classificationModel);

    return ClassificationJooqMapper.map(
            create.insertInto(CLASSIFICATIONS)
                    .set(classificationsRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public ClassificationModel updateClassification(long classificationId, ClassificationModel classificationModel) {
    ClassificationsRecord classificationsRecord = ClassificationJooqMapper.unmap(classificationModel);

    return ClassificationJooqMapper.map(
            create.update(CLASSIFICATIONS)
                    .set(classificationsRecord)
                    .where(CLASSIFICATIONS.ID.eq(classificationId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public ClassificationModel deleteClassification(long classificationId) {
    return ClassificationJooqMapper.map(
            create.delete(CLASSIFICATIONS)
                    .where(CLASSIFICATIONS.ID.eq(classificationId))
                    .returning()
                    .fetchOne()
    );
  }
}
