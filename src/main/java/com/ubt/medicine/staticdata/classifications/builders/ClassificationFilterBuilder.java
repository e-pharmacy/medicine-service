package com.ubt.medicine.staticdata.classifications.builders;

import com.ubt.medicine.staticdata.classifications.filters.ClassificationFilter;

import java.util.List;

public final class ClassificationFilterBuilder {
  private List<Long> ids;
  private String name;

  private ClassificationFilterBuilder() {
  }

  public static ClassificationFilterBuilder aClassificationFilter() {
    return new ClassificationFilterBuilder();
  }

  public ClassificationFilterBuilder withIds(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public ClassificationFilterBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public ClassificationFilter build() {
    ClassificationFilter classificationFilter = new ClassificationFilter();
    classificationFilter.setIds(ids);
    classificationFilter.setName(name);
    return classificationFilter;
  }
}
