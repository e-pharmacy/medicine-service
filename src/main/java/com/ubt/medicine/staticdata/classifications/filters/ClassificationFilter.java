package com.ubt.medicine.staticdata.classifications.filters;

import com.ubt.medicine.commons.filters.BaseFilter;
import com.ubt.medicine.commons.validation.ValidationService;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import static com.ubt.medicine.jooq.Tables.CLASSIFICATIONS;

public class ClassificationFilter extends BaseFilter {
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (ValidationService.notEmpty(getIds())) {
      condition = condition.and(CLASSIFICATIONS.ID.in(getIds()));
    }
    if (ValidationService.notBlank(getName())) {
      condition = condition.and(CLASSIFICATIONS.NAME.containsIgnoreCase(getName()));
    }

    return condition;
  }
}
