package com.ubt.medicine.staticdata.classifications;

import com.ubt.medicine.staticdata.classifications.filters.ClassificationFilter;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClassificationsRepository {
  Page<ClassificationModel> getClassifications(ClassificationFilter classificationFilter, Pageable pageable);

  ClassificationModel getClassificationById(long classificationId);

  ClassificationModel createClassification(ClassificationModel classificationModel);

  ClassificationModel updateClassification(long classificationId, ClassificationModel classificationModel);

  ClassificationModel deleteClassification(long classificationId);
}
