package com.ubt.medicine.drugs;

import com.ubt.medicine.commons.pageable.PageModel;
import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.drugs.filters.DrugFilter;
import com.ubt.medicine.drugs.jooqmappers.DrugJooqMapper;
import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.jooq.tables.records.DrugsRecord;
import com.ubt.medicine.jooq.tables.records.DrugsSideEffectsRecord;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep2;
import org.jooq.SortOrder;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.*;

@Repository
public class DrugsRepositoryImpl implements DrugsRepository {
  private final DSLContext create;

  @Autowired
  public DrugsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public PageModel<DrugModel> getDrugs(DrugFilter drugFilter, PageableModel pageableModel) {
    Condition condition = drugFilter.createFilterCondition();

    int totalElements = create.select(DSL.countDistinct(DRUGS.ID)).from(DRUGS)
            .leftOuterJoin(DRUGS_SIDE_EFFECTS).onKey()
            .where(condition)
            .fetchOneInto(Integer.class);

    pageableModel.getOrders().add(DSL.field("drugs.id").sort(SortOrder.DESC));

    List<DrugModel> drugModels = DrugJooqMapper.map(
            create.select().distinctOn(DRUGS.ID).from(DRUGS)
                    .leftOuterJoin(DRUGS_SIDE_EFFECTS).onKey()
                    .where(condition)
                    .orderBy(pageableModel.getOrders())
                    .offset(pageableModel.getOffset())
                    .limit(pageableModel.getLimit())
                    .fetchInto(DrugsRecord.class)
    );

    return PageableUtils.createPageModel(drugModels, totalElements);
  }

  @Override
  public DrugModel getDrugById(long drugId) {
    return DrugJooqMapper.map(
            create.selectFrom(DRUGS)
                    .where(DRUGS.ID.eq(drugId))
                    .fetchOne()
    );
  }

  @Override
  public DrugModel createDrug(DrugModel drugModel) {
    DrugsRecord drugsRecord = DrugJooqMapper.unmap(drugModel);

    return DrugJooqMapper.map(
            create.insertInto(DRUGS)
                    .set(drugsRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public DrugModel updateDrug(long drugId, DrugModel drugModel) {
    DrugsRecord drugsRecord = DrugJooqMapper.unmap(drugModel);

    return DrugJooqMapper.map(
            create.update(DRUGS)
                    .set(drugsRecord)
                    .where(DRUGS.ID.eq(drugId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public DrugModel deleteDrug(long drugId) {
    //Remove drug side effect references
    addDrugSideEffects(drugId, null);

    return DrugJooqMapper.map(
            create.delete(DRUGS)
                    .where(DRUGS.ID.eq(drugId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public void addDrugSideEffects(long drugId, List<Long> sideEffectIds) {
    //Remove previous side effects of drug
    create.deleteFrom(DRUGS_SIDE_EFFECTS)
            .where(DRUGS_SIDE_EFFECTS.DRUG_ID.eq(drugId))
            .execute();

    if (ValidationService.notEmpty(sideEffectIds)) {
      InsertValuesStep2<DrugsSideEffectsRecord, Long, Long> insert = create.insertInto(DRUGS_SIDE_EFFECTS, DRUGS_SIDE_EFFECTS.DRUG_ID, DRUGS_SIDE_EFFECTS.SIDE_EFFECT_ID);
      for (Long sideEffectId : sideEffectIds) {
        insert.values(drugId, sideEffectId);
      }
      insert.execute();
    }
  }
}
