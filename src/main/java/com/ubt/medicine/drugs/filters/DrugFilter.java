package com.ubt.medicine.drugs.filters;

import com.ubt.medicine.commons.filters.BaseFilter;
import com.ubt.medicine.commons.validation.ValidationService;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.DRUGS;
import static com.ubt.medicine.jooq.Tables.DRUGS_SIDE_EFFECTS;

public class DrugFilter extends BaseFilter {
  private String name;
  private List<Long> classificationsIds;
  private List<Long> categoriesIds;
  private List<Long> typesIds;
  private List<Long> sideEffectsIds;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Long> getClassificationsIds() {
    return classificationsIds;
  }

  public void setClassificationsIds(List<Long> classificationsIds) {
    this.classificationsIds = classificationsIds;
  }

  public List<Long> getCategoriesIds() {
    return categoriesIds;
  }

  public void setCategoriesIds(List<Long> categoriesIds) {
    this.categoriesIds = categoriesIds;
  }

  public List<Long> getTypesIds() {
    return typesIds;
  }

  public void setTypesIds(List<Long> typesIds) {
    this.typesIds = typesIds;
  }

  public List<Long> getSideEffectsIds() {
    return sideEffectsIds;
  }

  public void setSideEffectsIds(List<Long> sideEffectsIds) {
    this.sideEffectsIds = sideEffectsIds;
  }

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (ValidationService.notEmpty(getIds())) {
      condition = condition.and(DRUGS.ID.in(getIds()));
    }
    if (ValidationService.notBlank(getName())) {
      condition = condition.and(DRUGS.NAME.containsIgnoreCase(getName()));
    }
    if (ValidationService.notEmpty(getClassificationsIds())) {
      condition = condition.and(DRUGS.CLASSIFICATION_ID.in(getClassificationsIds()));
    }
    if (ValidationService.notEmpty(getCategoriesIds())) {
      condition = condition.and(DRUGS.CATEGORY_ID.in(getCategoriesIds()));
    }
    if (ValidationService.notEmpty(getSideEffectsIds())) {
      condition = condition.and(DRUGS_SIDE_EFFECTS.SIDE_EFFECT_ID.in(getSideEffectsIds()));
    }

    return condition;
  }
}
