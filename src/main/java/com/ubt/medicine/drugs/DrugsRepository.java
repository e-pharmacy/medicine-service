package com.ubt.medicine.drugs;

import com.ubt.medicine.commons.pageable.PageModel;
import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.drugs.filters.DrugFilter;
import com.ubt.medicine.drugs.models.DrugModel;

import java.util.List;

public interface DrugsRepository {
  PageModel<DrugModel> getDrugs(DrugFilter drugFilter, PageableModel pageableModel);

  DrugModel getDrugById(long drugId);

  DrugModel createDrug(DrugModel drugModel);

  DrugModel updateDrug(long drugId, DrugModel drugModel);

  DrugModel deleteDrug(long drugId);

  void addDrugSideEffects(long drugId, List<Long> sideEffectIds);
}
