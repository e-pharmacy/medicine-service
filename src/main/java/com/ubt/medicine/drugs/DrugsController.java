package com.ubt.medicine.drugs;

import com.ubt.medicine.drugs.filters.DrugFilter;
import com.ubt.medicine.drugs.modelexpanders.DrugModelExpander;
import com.ubt.medicine.drugs.models.DrugModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/drugs")
public class DrugsController {
  private final DrugsService drugsService;

  @Autowired
  public DrugsController(DrugsService drugsService) {
    this.drugsService = drugsService;
  }

  @GetMapping("")
  public Page<DrugModel> getDrugs(DrugFilter drugFilter, Pageable pageable, @RequestParam(required = false) List<String> expand) {
    return drugsService.getDrugs(drugFilter, pageable, expand);
  }

  @GetMapping("/{drugId}")
  public DrugModel getDrugById(@PathVariable long drugId) {
    return drugsService.getDrugById(drugId, List.of(DrugModelExpander.ALL));
  }

  @PostMapping("")
  public DrugModel createDrug(@RequestBody DrugModel drugModel) {
    return drugsService.createDrug(drugModel);
  }

  @PutMapping("/{drugId}")
  public DrugModel updateDrug(@PathVariable long drugId, @RequestBody DrugModel drugModel) {
    return drugsService.updateDrug(drugId, drugModel);
  }

  @DeleteMapping("/{drugId}")
  public DrugModel deleteDrug(@PathVariable long drugId) {
    return drugsService.deleteDrug(drugId);
  }

  @PostMapping("/{drugId}/side-effects")
  @ResponseStatus(HttpStatus.CREATED)
  public void addDrugSideEffects(@PathVariable long drugId, @RequestParam(required = false) List<Long> sideEffectIds) {
    drugsService.addDrugSideEffects(drugId, sideEffectIds);
  }
}
