package com.ubt.medicine.drugs;

import com.ubt.medicine.commons.pageable.PageModel;
import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.drugs.filters.DrugFilter;
import com.ubt.medicine.drugs.modelexpanders.DrugModelExpander;
import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.staticdata.categories.CategoriesService;
import com.ubt.medicine.staticdata.categories.builders.CategoryFilterBuilder;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import com.ubt.medicine.staticdata.classifications.ClassificationsService;
import com.ubt.medicine.staticdata.classifications.builders.ClassificationFilterBuilder;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import com.ubt.medicine.staticdata.sideeffects.SideEffectsService;
import com.ubt.medicine.staticdata.sideeffects.builders.SideEffectFilterBuilder;
import com.ubt.medicine.staticdata.sideeffects.models.SideEffectModel;
import com.ubt.medicine.staticdata.types.TypesService;
import com.ubt.medicine.staticdata.types.builders.TypeFilterBuilder;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugsServiceImpl implements DrugsService {
  private final DrugsRepository drugsRepository;
  private final DrugModelExpander drugModelExpander;

  @Autowired
  public DrugsServiceImpl(DrugsRepository drugsRepository, DrugModelExpander drugModelExpander) {
    this.drugsRepository = drugsRepository;
    this.drugModelExpander = drugModelExpander;
  }

  @Override
  public Page<DrugModel> getDrugs(DrugFilter drugFilter, Pageable pageable, List<String> expand) {
    PageableModel pageableModel = PageableUtils.createPageableModel(pageable);
    PageModel<DrugModel> pageModel = drugsRepository.getDrugs(drugFilter, pageableModel);

    drugModelExpander.expandSubObjects(pageModel.getContent(), expand);

    return PageableUtils.newPage(pageModel.getContent(), pageableModel, pageModel.getTotalElements());
  }

  @Override
  public DrugModel getDrugById(long drugId, List<String> expand) {
    DrugModel drugModel = drugsRepository.getDrugById(drugId);

    drugModelExpander.expandSubObjects(drugModel, expand);

    return drugModel;
  }

  @Override
  public DrugModel createDrug(DrugModel drugModel) {
    return drugsRepository.createDrug(drugModel);
  }

  @Override
  public DrugModel updateDrug(long drugId, DrugModel drugModel) {
    return drugsRepository.updateDrug(drugId, drugModel);
  }

  @Override
  public DrugModel deleteDrug(long drugId) {
    return drugsRepository.deleteDrug(drugId);
  }

  @Override
  public void addDrugSideEffects(long drugId, List<Long> sideEffectIds) {
    drugsRepository.addDrugSideEffects(drugId, sideEffectIds);
  }
}
