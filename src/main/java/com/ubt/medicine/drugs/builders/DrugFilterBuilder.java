package com.ubt.medicine.drugs.builders;

import com.ubt.medicine.drugs.filters.DrugFilter;

import java.util.List;

public final class DrugFilterBuilder {
  private List<Long> drugsIds;
  private String name;
  private List<Long> classificationsIds;
  private List<Long> categoriesIds;
  private List<Long> sideEffectsIds;

  private DrugFilterBuilder() {
  }

  public static DrugFilterBuilder aDrugFilter() {
    return new DrugFilterBuilder();
  }

  public DrugFilterBuilder withDrugsIds(List<Long> drugsIds) {
    this.drugsIds = drugsIds;
    return this;
  }

  public DrugFilterBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public DrugFilterBuilder withClassificationsIds(List<Long> classificationsIds) {
    this.classificationsIds = classificationsIds;
    return this;
  }

  public DrugFilterBuilder withCategoriesIds(List<Long> categoriesIds) {
    this.categoriesIds = categoriesIds;
    return this;
  }

  public DrugFilterBuilder withSideEffectsIds(List<Long> sideEffectsIds) {
    this.sideEffectsIds = sideEffectsIds;
    return this;
  }

  public DrugFilter build() {
    DrugFilter drugFilter = new DrugFilter();
    drugFilter.setIds(drugsIds);
    drugFilter.setName(name);
    drugFilter.setClassificationsIds(classificationsIds);
    drugFilter.setCategoriesIds(categoriesIds);
    drugFilter.setSideEffectsIds(sideEffectsIds);
    return drugFilter;
  }
}
