package com.ubt.medicine.drugs.builders;

import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;

public final class DrugModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private String name;
  private Long classificationId;
  private Long categoryId;
  private ClassificationModel classification;
  private CategoryModel category;

  private DrugModelBuilder() {
  }

  public static DrugModelBuilder aDrugModel() {
    return new DrugModelBuilder();
  }

  public DrugModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public DrugModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public DrugModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public DrugModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public DrugModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public DrugModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public DrugModelBuilder withClassificationId(Long classificationId) {
    this.classificationId = classificationId;
    return this;
  }

  public DrugModelBuilder withCategoryId(Long categoryId) {
    this.categoryId = categoryId;
    return this;
  }

  public DrugModelBuilder withClassification(ClassificationModel classification) {
    this.classification = classification;
    return this;
  }

  public DrugModelBuilder withCategory(CategoryModel category) {
    this.category = category;
    return this;
  }

  public DrugModel build() {
    DrugModel drugModel = new DrugModel();
    drugModel.setId(id);
    drugModel.setCreatedDate(createdDate);
    drugModel.setUpdatedDate(updatedDate);
    drugModel.setCreatedBy(createdBy);
    drugModel.setUpdatedBy(updatedBy);
    drugModel.setName(name);
    drugModel.setClassificationId(classificationId);
    drugModel.setCategoryId(categoryId);
    drugModel.setClassification(classification);
    drugModel.setCategory(category);
    return drugModel;
  }
}
