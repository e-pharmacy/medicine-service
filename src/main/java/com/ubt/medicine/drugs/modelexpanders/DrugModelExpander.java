package com.ubt.medicine.drugs.modelexpanders;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.staticdata.categories.CategoriesService;
import com.ubt.medicine.staticdata.categories.builders.CategoryFilterBuilder;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import com.ubt.medicine.staticdata.classifications.ClassificationsService;
import com.ubt.medicine.staticdata.classifications.builders.ClassificationFilterBuilder;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import com.ubt.medicine.staticdata.sideeffects.SideEffectsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DrugModelExpander {
  /**
   * Sub objects field name constants
   */
  public static final String ALL = "all";
  public static final String CLASSIFICATION = "classification";
  public static final String CATEGORY = "category";
  private final ClassificationsService classificationsService;
  private final CategoriesService categoriesService;
  private final SideEffectsService sideEffectsService;

  @Autowired
  public DrugModelExpander(ClassificationsService classificationsService, CategoriesService categoriesService, SideEffectsService sideEffectsService) {
    this.classificationsService = classificationsService;
    this.categoriesService = categoriesService;
    this.sideEffectsService = sideEffectsService;
  }

  /**
   * @param drugs
   * @param expand
   */
  public void expandSubObjects(List<DrugModel> drugs, List<String> expand) {
    if (ValidationService.notEmpty(drugs) && ValidationService.notEmpty(expand)) {
      boolean expandAll = expand.contains(ALL);

      Map<Long, ClassificationModel> classificationsMap = new HashMap<>();
      Map<Long, CategoryModel> categoriesMap = new HashMap<>();

      if (expand.contains(CLASSIFICATION) || expandAll) {
        classificationsMap = getClassifications(drugs);
      }
      if (expand.contains(CATEGORY) || expandAll) {
        categoriesMap = getCategories(drugs);
      }

      Map<Long, ClassificationModel> finalClassificationsMap = classificationsMap;
      Map<Long, CategoryModel> finalCategoriesMap = categoriesMap;

      drugs.forEach(drugModel -> {
        drugModel.setClassification(finalClassificationsMap.get(drugModel.getClassificationId()));
        drugModel.setCategory(finalCategoriesMap.get(drugModel.getCategoryId()));
      });
    }
  }

  /**
   * @param drugModel
   * @param expand
   */
  public void expandSubObjects(DrugModel drugModel, List<String> expand) {
    if (Objects.nonNull(drugModel) && ValidationService.notEmpty(expand)) {
      boolean expandAll = expand.contains(ALL);

      if (expand.contains(CLASSIFICATION) || expandAll) {
        drugModel.setClassification(getClassification(drugModel));
      }
      if (expand.contains(CATEGORY) || expandAll) {
        drugModel.setCategory(getCategory(drugModel));
      }
    }
  }

  public Map<Long, ClassificationModel> getClassifications(List<DrugModel> content) {
    List<Long> classificationsIds = content.stream().map(DrugModel::getClassificationId).collect(Collectors.toList());
    List<ClassificationModel> classifications = classificationsService.getClassifications(
                    ClassificationFilterBuilder.aClassificationFilter()
                            .withIds(classificationsIds).build(), null)
            .getContent();
    return classifications.stream().collect(Collectors.toMap(ClassificationModel::getId, c -> c));
  }

  public Map<Long, CategoryModel> getCategories(List<DrugModel> content) {
    List<Long> categoriesIds = content.stream().map(DrugModel::getCategoryId).collect(Collectors.toList());
    List<CategoryModel> categories = categoriesService.getCategories(
                    CategoryFilterBuilder.aCategoryFilter()
                            .withIds(categoriesIds).build(), null)
            .getContent();
    return categories.stream().collect(Collectors.toMap(CategoryModel::getId, c -> c));
  }

  public ClassificationModel getClassification(DrugModel drugModel) {
    return classificationsService.getClassificationById(drugModel.getClassificationId());
  }

  public CategoryModel getCategory(DrugModel drugModel) {
    return categoriesService.getCategoryById(drugModel.getCategoryId());
  }
}
