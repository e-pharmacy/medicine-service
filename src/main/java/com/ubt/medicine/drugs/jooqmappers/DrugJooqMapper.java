package com.ubt.medicine.drugs.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.drugs.builders.DrugModelBuilder;
import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.jooq.tables.records.DrugsRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DrugJooqMapper {
  public static DrugModel map(DrugsRecord record) {
    DrugModel model = null;
    if (record != null) {
      model = DrugModelBuilder.aDrugModel()
              .withId(record.getId())
              .withName(record.getName())
              .withClassificationId(record.getClassificationId())
              .withCategoryId(record.getCategoryId())
              .build();
    }
    return model;
  }

  public static DrugsRecord unmap(DrugModel model) {
    DrugsRecord record = new DrugsRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getName())) {
      record.setName(model.getName());
    }
    if (Objects.nonNull(model.getClassificationId())) {
      record.setClassificationId(model.getClassificationId());
    }
    if (Objects.nonNull(model.getCategoryId())) {
      record.setCategoryId(model.getCategoryId());
    }

    return record;
  }

  public static List<DrugModel> map(List<DrugsRecord> records) {
    List<DrugModel> models = new ArrayList<>();

    for (DrugsRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
