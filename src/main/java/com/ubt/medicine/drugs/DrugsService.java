package com.ubt.medicine.drugs;

import com.ubt.medicine.drugs.filters.DrugFilter;
import com.ubt.medicine.drugs.models.DrugModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Validated
public interface DrugsService {
  Page<DrugModel> getDrugs(DrugFilter drugFilter, Pageable pageable, List<String> expand);

  DrugModel getDrugById(long drugId, List<String> expand);

  DrugModel createDrug(@Valid DrugModel drugModel);

  DrugModel updateDrug(long drugId, @Valid DrugModel drugModel);

  DrugModel deleteDrug(long drugId);

  void addDrugSideEffects(long drugId, List<Long> sideEffectIds);
}
