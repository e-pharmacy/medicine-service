package com.ubt.medicine.drugs.models;

import com.ubt.medicine.commons.models.BoBaseModel;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DrugModel extends BoBaseModel {
  @NotBlank(message = "{drug.name.notBlank}")
  private String name;
  @NotNull(message = "{drug.classificationId.notNull}")
  private Long classificationId;
  @NotNull(message = "{drug.categoryId.notNull}")
  private Long categoryId;
  /**
   * Sub object fields
   */
  private ClassificationModel classification;
  private CategoryModel category;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(Long classificationId) {
    this.classificationId = classificationId;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public ClassificationModel getClassification() {
    return classification;
  }

  public void setClassification(ClassificationModel classification) {
    this.classification = classification;
  }

  public CategoryModel getCategory() {
    return category;
  }

  public void setCategory(CategoryModel category) {
    this.category = category;
  }
}
