package com.ubt.medicine.drugs.validation;

public interface ValidationMessages {
  String BLANK_NAME = "name must not be blank";
  String BLANK_DESCRIPTION = "description must not be blank";
  String BLANK_WARNING = "warning must not be blank";
  String BLANK_PRESCRIPTION = "prescription must not be blank";
  String NULL_CLASSIFICATION_ID = "classification_id must not be null";
  String NULL_CATEGORY_ID = "category_id must not be null";
  String NULL_TYPE_ID = "type_id must not be null";
}
