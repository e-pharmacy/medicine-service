package com.ubt.medicine.filestorage;

import com.ubt.medicine.commons.exceptions.BadRequestException;
import com.ubt.medicine.commons.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class StorageServiceImpl implements StorageService {
  private final Path rootLocation;

  @Autowired
  public StorageServiceImpl(StorageProperties properties) {
    this.rootLocation = Paths.get(properties.getDir());
  }

  @Override
  public void store(MultipartFile file, String newFilename) {
    String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

    try {
      if (file.isEmpty()) {
        throw new BadRequestException("Failed to store empty file " + filename);
      }
      if (filename.contains("..")) {
        // This is a security check
        throw new BadRequestException("Cannot store file with relative path outside current directory "
            + filename);
      }
      String path = rootLocation.resolve(newFilename).toString();
      file.transferTo(new File(path));
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  @Override
  public Stream<Path> loadAll() {
    try {
      return Files.walk(rootLocation, 1)
          .filter(path -> !path.equals(rootLocation))
          .map(rootLocation::relativize);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Failed to read stored files", e);
    }

  }

  @Override
  public Path load(String filename) {
    return rootLocation.resolve(filename);
  }

  @Override
  public Resource loadAsResource(String filename) {
    try {
      Path file = load(rootLocation + "/" + filename);
      Resource resource = new UrlResource(file.toUri());
      if (!resource.exists()) {
        throw new NotFoundException("Could not find file: " + filename);
      } else if (!resource.isReadable()) {
        throw new RuntimeException("Could not read file: " + filename);
      } else {
        return resource;
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("Could not read file: " + filename);
    }
  }

  @Override
  public void deleteAll() {
    FileSystemUtils.deleteRecursively(rootLocation.toFile());
  }

  @Override
  public void delete(String filename) {
    if (filename == null) return;
    String simpleFileName = filename.split("\\.")[0];
    File folder = rootLocation.toFile();
    final File[] files = folder.listFiles((dir, name) ->
        name.matches(simpleFileName + "\\.[A-Za-z1-9]+")
    );

    if (files != null) {
      for (final File file : files) {
        if (!file.delete()) {
          throw new RuntimeException("Could not delete file");
        }
      }
    }
  }

  @Override
  public void init() {
    try {
      Files.createDirectories(rootLocation);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Could not initialize storage", e);
    }
  }
}
