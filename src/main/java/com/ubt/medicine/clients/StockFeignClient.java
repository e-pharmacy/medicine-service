package com.ubt.medicine.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "stock-service", url = "${feign.address.stock-service}")
public interface StockFeignClient {
  @GetMapping("supplies/quantity")
  Integer getProductQuantityByProductId(@RequestParam Long productId);
}