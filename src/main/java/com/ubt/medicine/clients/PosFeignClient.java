package com.ubt.medicine.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "pos-service", url = "${feign.address.pos-service}")
public interface PosFeignClient {
  @GetMapping(value = "/sales", params = {"productId"})
  Integer getProductSalesCount(@RequestParam Long productId);
}
