package com.ubt.medicine.commons.validation.annotations;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class ImageValidator implements ConstraintValidator<Image, MultipartFile> {
  private static final List<String> ALLOWED_MEDIA_TYPES = List.of("jpg", "jpeg", "png");

  @Override
  public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
    if (value == null) return false;
    String extension = FilenameUtils.getExtension(value.getOriginalFilename());
    if (extension == null) return false;
    return ALLOWED_MEDIA_TYPES.contains(extension.toLowerCase());
  }
}
