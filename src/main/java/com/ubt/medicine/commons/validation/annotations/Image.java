package com.ubt.medicine.commons.validation.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The annotated element must be a file that ends with one of the extensions:
 * (jpg, jpeg, png)
 * Accepts org.springframework.web.multipart.MultipartFile.
 * Null is considered invalid.
 */
@Target({PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = ImageValidator.class)
@Documented
public @interface Image {
  String message() default "{Image.invalid}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}