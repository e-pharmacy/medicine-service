package com.ubt.medicine.commons.exceptions;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class CustomGlobalExceptionHandler {
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> errorHandler(HttpServletRequest request, Exception ex) throws Exception {
    ResponseStatus responseStatusAnnotation = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
    HttpStatus status;
    String message;
    //ConstraintViolationException is thrown from @Valid and may have many messages
    if (ex instanceof ConstraintViolationException) {
      status = HttpStatus.BAD_REQUEST;
      //only get the first message
      message = new ArrayList<>(((ConstraintViolationException) ex).getConstraintViolations())
              .get(0).getMessage();

      return buildExceptionResponse(status, message, request.getServletPath());
    } else if (responseStatusAnnotation != null) {
      status = responseStatusAnnotation.value();
      message = ex.getMessage();

      return buildExceptionResponse(status, message, request.getServletPath());
    } else {
      throw ex;
    }
  }

  private ResponseEntity<Object> buildExceptionResponse(HttpStatus status, String message, String path) {
    Map<String, Object> body = new LinkedHashMap<>();
    body.put("timestamp", new Date());
    body.put("status", status.value());
    body.put("error", status.getReasonPhrase());
    body.put("message", message != null ? message : "No message available");
    body.put("path", path);

    return new ResponseEntity<>(body, status);
  }
}