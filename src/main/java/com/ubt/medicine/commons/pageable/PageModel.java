package com.ubt.medicine.commons.pageable;

import com.ubt.medicine.commons.validation.ValidationService;

import java.util.ArrayList;
import java.util.List;

public class PageModel<E> {
  private List<E> content;
  private Integer totalElements;

  public List<E> getContent() {
    return content;
  }

  protected PageModel(List<E> content, Integer totalElements) {
    this.content = ValidationService.isEmpty(content)? new ArrayList<>() : content;
    this.totalElements = totalElements;
  }

  public void setContent(List<E> content) {
    this.content = content;
  }

  public Integer getTotalElements() {
    return totalElements;
  }

  public void setTotalElements(Integer totalElements) {
    this.totalElements = totalElements;
  }
}
