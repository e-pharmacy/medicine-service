package com.ubt.medicine.commons.pageable;

import org.jooq.SortField;
import org.jooq.SortOrder;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.jooq.impl.DSL.field;

/**
 * Utility class for pagination functionality
 */
public class PageableUtils {
  public static List<SortField<?>> convertSortFields(Sort sort) {
    List<SortField<?>> orders = new ArrayList<>();
    //add every element from sort to orders
    for (Sort.Order order : sort) {
      orders.add(field(order.getProperty()).sort(order.getDirection() == Sort.Direction.ASC ? SortOrder.ASC : SortOrder.DESC));
    }
    return orders;
  }

  public static PageableModel createPageableModel(Pageable pageable) {
    return new PageableModel(pageable);
  }

  public static <T> PageModel<T> createPageModel(List<T> content, Integer totalElements) {
    return new PageModel<T>(content, totalElements);
  }

  public static <T> PageImpl<T> newPage(List<T> details, PageableModel pageableModel, int totalElements) {
    return new PageImpl<>(details, pageableModel.getPageable(), totalElements);
  }
}
