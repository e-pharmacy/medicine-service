package com.ubt.medicine.manageproducts.builders;

import com.ubt.medicine.manageproducts.models.PictureModel;

public final class PictureModelBuilder {
  private String filename;
  private Long sizeInBytes;

  private PictureModelBuilder() {
  }

  public static PictureModelBuilder aPictureModel() {
    return new PictureModelBuilder();
  }

  public PictureModelBuilder withFilename(String filename) {
    this.filename = filename;
    return this;
  }

  public PictureModelBuilder withSizeInBytes(Long sizeInBytes) {
    this.sizeInBytes = sizeInBytes;
    return this;
  }

  public PictureModel build() {
    PictureModel pictureModel = new PictureModel();
    pictureModel.setFilename(filename);
    pictureModel.setSizeInBytes(sizeInBytes);
    return pictureModel;
  }
}
