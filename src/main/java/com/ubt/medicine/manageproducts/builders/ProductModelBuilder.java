/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 01/10/2021.
 */

package com.ubt.medicine.manageproducts.builders;

import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.manageproducts.models.ProductModel;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import com.ubt.medicine.staticdata.types.models.TypeModel;

public final class ProductModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private Long drugId;
  private String imagePath;
  private Long producerCountryId;
  private String brandName;
  private String description;
  private String warning;
  private String prescription;
  //TODO generate uuid or something
  private Integer barcode;
  private Long typeId;
  private Boolean recommended;
  private Double pricePerUnit;
  private Integer quantityLeftInStock;
  private CountryModel producerCountry;
  private TypeModel type;
  private DrugModel drug;

  private ProductModelBuilder() {
  }

  public static ProductModelBuilder aProductModel() {
    return new ProductModelBuilder();
  }

  public ProductModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public ProductModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public ProductModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public ProductModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public ProductModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public ProductModelBuilder withDrugId(Long drugId) {
    this.drugId = drugId;
    return this;
  }

  public ProductModelBuilder withImagePath(String imagePath) {
    this.imagePath = imagePath;
    return this;
  }

  public ProductModelBuilder withProducerCountryId(Long producerCountryId) {
    this.producerCountryId = producerCountryId;
    return this;
  }

  public ProductModelBuilder withBrandName(String brandName) {
    this.brandName = brandName;
    return this;
  }

  public ProductModelBuilder withDescription(String description) {
    this.description = description;
    return this;
  }

  public ProductModelBuilder withWarning(String warning) {
    this.warning = warning;
    return this;
  }

  public ProductModelBuilder withPrescription(String prescription) {
    this.prescription = prescription;
    return this;
  }

  public ProductModelBuilder withBarcode(Integer barcode) {
    this.barcode = barcode;
    return this;
  }

  public ProductModelBuilder withTypeId(Long typeId) {
    this.typeId = typeId;
    return this;
  }

  public ProductModelBuilder withRecommended(Boolean recommended) {
    this.recommended = recommended;
    return this;
  }

  public ProductModelBuilder withPricePerUnit(Double pricePerUnit) {
    this.pricePerUnit = pricePerUnit;
    return this;
  }

  public ProductModelBuilder withQuantityLeftInStock(Integer quantityLeftInStock) {
    this.quantityLeftInStock = quantityLeftInStock;
    return this;
  }

  public ProductModelBuilder withProducerCountry(CountryModel producerCountry) {
    this.producerCountry = producerCountry;
    return this;
  }

  public ProductModelBuilder withType(TypeModel type) {
    this.type = type;
    return this;
  }

  public ProductModelBuilder withDrug(DrugModel drug) {
    this.drug = drug;
    return this;
  }

  public ProductModel build() {
    ProductModel productModel = new ProductModel();
    productModel.setId(id);
    productModel.setCreatedDate(createdDate);
    productModel.setUpdatedDate(updatedDate);
    productModel.setCreatedBy(createdBy);
    productModel.setUpdatedBy(updatedBy);
    productModel.setDrugId(drugId);
    productModel.setImagePath(imagePath);
    productModel.setProducerCountryId(producerCountryId);
    productModel.setBrandName(brandName);
    productModel.setDescription(description);
    productModel.setWarning(warning);
    productModel.setPrescription(prescription);
    productModel.setBarcode(barcode);
    productModel.setTypeId(typeId);
    productModel.setRecommended(recommended);
    productModel.setPricePerUnit(pricePerUnit);
    productModel.setQuantityLeftInStock(quantityLeftInStock);
    productModel.setProducerCountry(producerCountry);
    productModel.setType(type);
    productModel.setDrug(drug);
    return productModel;
  }
}
