/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.medicine.manageproducts;

import com.ubt.medicine.manageproducts.filters.ProductFilter;
import com.ubt.medicine.manageproducts.modelexpanders.ProductModelExpander;
import com.ubt.medicine.manageproducts.models.PictureModel;
import com.ubt.medicine.manageproducts.models.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ManageProductsController {
  private final ManageProductsService manageProductsService;

  @Autowired
  public ManageProductsController(ManageProductsService manageProductsService) {
    this.manageProductsService = manageProductsService;
  }

  @GetMapping(params = {"barcode"})
  public ProductModel getProductByBarcode(@RequestParam Integer barcode) {
    return manageProductsService.getProductByBarcode(barcode);
  }

  @GetMapping("")
  public Page<ProductModel> getProducts(ProductFilter productFilter, Pageable pageable, @RequestParam(required = false) List<String> expand) {
    return manageProductsService.getProducts(productFilter, pageable, expand);
  }

  @GetMapping(value = "", params = {"unPaged=true"})
  public List<ProductModel> getProducts(ProductFilter productFilter, @RequestParam(required = false) List<String> expand) {
    return manageProductsService.getProducts(productFilter, Pageable.unpaged(), expand).getContent();
  }

  @GetMapping("/{productId}")
  public ProductModel getProductById(@PathVariable long productId) {
    return manageProductsService.getProductById(productId, List.of(ProductModelExpander.ALL));
  }

  @PostMapping("")
  public ProductModel createProduct(@RequestBody ProductModel productModel) {
    return manageProductsService.createProduct(productModel);
  }

  @PutMapping("/{productId}")
  public ProductModel updateProduct(@PathVariable long productId, @RequestBody ProductModel productModel) {
    return manageProductsService.updateProduct(productId, productModel);
  }

  @DeleteMapping("/{productId}")
  public ProductModel deleteProduct(@PathVariable long productId) {
    return manageProductsService.deleteProduct(productId);
  }

  @GetMapping("/picture/{filename:.+}")
  public ResponseEntity<Resource> getProfilePicture(@PathVariable String filename, @SpringQueryMap HttpServletRequest request) {
    return manageProductsService.getProfilePicture(filename, request);
  }

  @PostMapping("/{productId}/picture")
  public PictureModel updateProfilePicture(@PathVariable long productId, @RequestParam("file") MultipartFile file) {
    return manageProductsService.updateProfilePicture(productId, file);
  }

  @DeleteMapping("/{productId}/picture")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteProfilePicture(@PathVariable long productId) {
    manageProductsService.deleteProfilePicture(productId);
  }
}
