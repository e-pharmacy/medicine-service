package com.ubt.medicine.manageproducts.modelexpanders;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.drugs.DrugsService;
import com.ubt.medicine.drugs.builders.DrugFilterBuilder;
import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.manageproducts.models.ProductModel;
import com.ubt.medicine.staticdata.categories.builders.CategoryFilterBuilder;
import com.ubt.medicine.staticdata.categories.models.CategoryModel;
import com.ubt.medicine.staticdata.classifications.builders.ClassificationFilterBuilder;
import com.ubt.medicine.staticdata.classifications.models.ClassificationModel;
import com.ubt.medicine.staticdata.countries.CountriesService;
import com.ubt.medicine.staticdata.countries.builders.CountryFilterBuilder;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import com.ubt.medicine.staticdata.types.TypesService;
import com.ubt.medicine.staticdata.types.builders.TypeFilterBuilder;
import com.ubt.medicine.staticdata.types.models.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProductModelExpander {
  private final CountriesService countriesService;
  private final TypesService typesService;
  private final DrugsService drugsService;

  @Autowired
  public ProductModelExpander(CountriesService countriesService, TypesService typesService, DrugsService drugsService) {
    this.countriesService = countriesService;
    this.typesService = typesService;
    this.drugsService = drugsService;
  }

  /**
   * Sub objects field name constants
   */
  public static final String ALL = "all";
  public static final String PRODUCER_COUNTRY = "producer_country";
  public static final String TYPE = "type";
  public static final String DRUG = "drug";

  /**
   *
   * @param products
   * @param expand
   */
  public void expandSubObjects(List<ProductModel> products, List<String> expand) {
    if (ValidationService.notEmpty(products) && ValidationService.notEmpty(expand)) {
      boolean expandAll = expand.contains(ALL);

      Map<Long, CountryModel> countriesMap = new HashMap<>();
      Map<Long, TypeModel> typesMap = new HashMap<>();
      Map<Long, DrugModel> drugsMap = new HashMap<>();

      if (expand.contains(PRODUCER_COUNTRY) || expandAll) {
        countriesMap = getCountries(products);
      }
      if (expand.contains(TYPE) || expandAll) {
        typesMap = getTypes(products);
      }
      if (expand.contains(DRUG) || expandAll) {
        drugsMap = getDrugs(products);
      }

      Map<Long, CountryModel> finalCountriesMap = countriesMap;
      Map<Long, TypeModel> finalTypesMap = typesMap;
      Map<Long, DrugModel> finalDrugsMap = drugsMap;

      products.forEach(productModel -> {
        productModel.setProducerCountry(finalCountriesMap.get(productModel.getProducerCountryId()));
        productModel.setType(finalTypesMap.get(productModel.getTypeId()));
        productModel.setDrug(finalDrugsMap.get(productModel.getDrugId()));
      });
    }
  }

  /**
   *
   * @param productModel
   * @param expand
   */
  public void expandSubObjects(ProductModel productModel, List<String> expand) {
    if (Objects.nonNull(productModel) && ValidationService.notEmpty(expand)) {
      boolean expandAll = expand.contains(ALL);

      if (expand.contains(PRODUCER_COUNTRY) || expandAll) {
        productModel.setProducerCountry(getCountry(productModel));
      }
      if (expand.contains(TYPE) || expandAll) {
        productModel.setType(getType(productModel));
      }
      if (expand.contains(DRUG) || expandAll) {
        productModel.setDrug(getDrug(productModel));
      }
    }
  }

  public Map<Long, CountryModel> getCountries(List<ProductModel> content) {
    List<Long> countriesIds = content.stream().map(ProductModel::getProducerCountryId).collect(Collectors.toList());
    List<CountryModel> countries = countriesService.getCountries(
            CountryFilterBuilder.aCountryFilter()
                    .withIds(countriesIds).build(), null)
            .getContent();
    return countries.stream().collect(Collectors.toMap(CountryModel::getId, c -> c));
  }

  public Map<Long, TypeModel> getTypes(List<ProductModel> content) {
    List<Long> typesIds = content.stream().map(ProductModel::getTypeId).collect(Collectors.toList());
    List<TypeModel> types = typesService.getTypes(
            TypeFilterBuilder.aTypeFilter()
                    .withIds(typesIds).build(), null)
            .getContent();
    return types.stream().collect(Collectors.toMap(TypeModel::getId, t -> t));
  }

  public Map<Long, DrugModel> getDrugs(List<ProductModel> content) {
    List<Long> drugsIds = content.stream().map(ProductModel::getDrugId).collect(Collectors.toList());
    List<DrugModel> drugs = drugsService.getDrugs(
            DrugFilterBuilder.aDrugFilter()
                    .withDrugsIds(drugsIds).build(), null, null)
            .getContent();
    return drugs.stream().collect(Collectors.toMap(DrugModel::getId, t -> t));
  }

  public CountryModel getCountry(ProductModel productModel) {
    return countriesService.getCountryById(productModel.getProducerCountryId());
  }

  public TypeModel getType(ProductModel productModel) {
    return typesService.getTypeById(productModel.getTypeId());
  }

  public DrugModel getDrug(ProductModel productModel) {
    return drugsService.getDrugById(productModel.getDrugId(), null);
  }
}
