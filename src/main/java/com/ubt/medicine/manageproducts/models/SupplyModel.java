package com.ubt.medicine.manageproducts.models;

import com.ubt.medicine.commons.models.BoBaseModel;

public class SupplyModel extends BoBaseModel {
  private Long pharmacyLocationId;
  private Double buyPricePerUnit;
  private Double sellPricePerUnit;
  private Double mass;
  private Long expirationDate;
  private Integer barCode;
  private Long productId;
  private ProductModel productModel;

  public Long getPharmacyLocationId() {
    return pharmacyLocationId;
  }

  public void setPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
  }

  public Double getBuyPricePerUnit() {
    return buyPricePerUnit;
  }

  public void setBuyPricePerUnit(Double buyPricePerUnit) {
    this.buyPricePerUnit = buyPricePerUnit;
  }

  public Double getSellPricePerUnit() {
    return sellPricePerUnit;
  }

  public void setSellPricePerUnit(Double sellPricePerUnit) {
    this.sellPricePerUnit = sellPricePerUnit;
  }

  public Double getMass() {
    return mass;
  }

  public void setMass(Double mass) {
    this.mass = mass;
  }

  public Long getExpirationDate() {
    return expirationDate;
  }

  public void setExpirationDate(Long expirationDate) {
    this.expirationDate = expirationDate;
  }

  public Integer getBarCode() {
    return barCode;
  }

  public void setBarCode(Integer barCode) {
    this.barCode = barCode;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public ProductModel getProductModel() {
    return productModel;
  }

  public void setProductModel(ProductModel productModel) {
    this.productModel = productModel;
  }
}
