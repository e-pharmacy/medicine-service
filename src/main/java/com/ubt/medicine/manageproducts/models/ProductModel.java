package com.ubt.medicine.manageproducts.models;

import com.ubt.medicine.commons.models.BoBaseModel;
import com.ubt.medicine.drugs.models.DrugModel;
import com.ubt.medicine.staticdata.countries.models.CountryModel;
import com.ubt.medicine.staticdata.types.models.TypeModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProductModel extends BoBaseModel {
  @NotNull(message = "{product.drugId.notNull}")
  private Long drugId;
  private String imagePath;
  @NotNull(message = "{product.producerCountryId.notNull}")
  private Long producerCountryId;
  @NotBlank(message = "{product.brandName.notBlank}")
  private String brandName;
  @NotBlank(message = "{product.description.notBlank}")
  private String description;
  @NotBlank(message = "{product.warning.notBlank}")
  private String warning;
  @NotBlank(message = "{product.prescription.notBlank}")
  private String prescription;
  //TODO generate uuid or something
  private Integer barcode;
  @NotNull(message = "{product.typeId.notNull}")
  private Long typeId;
  private Boolean recommended;
  @NotNull(message = "{product.pricePerUnit.notNull}")
  private Double pricePerUnit;
  private Integer quantityLeftInStock;
  /**
   * Sub object fields
   */
  private CountryModel producerCountry;
  private TypeModel type;
  private DrugModel drug;

  public Long getDrugId() {
    return drugId;
  }

  public void setDrugId(Long drugId) {
    this.drugId = drugId;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public Long getProducerCountryId() {
    return producerCountryId;
  }

  public void setProducerCountryId(Long producerCountryId) {
    this.producerCountryId = producerCountryId;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getWarning() {
    return warning;
  }

  public void setWarning(String warning) {
    this.warning = warning;
  }

  public String getPrescription() {
    return prescription;
  }

  public void setPrescription(String prescription) {
    this.prescription = prescription;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public Integer getBarcode() {
    return barcode;
  }

  public void setBarcode(Integer barcode) {
    this.barcode = barcode;
  }

  public Boolean getRecommended() {
    return recommended;
  }

  public void setRecommended(Boolean recommended) {
    this.recommended = recommended;
  }

  public Double getPricePerUnit() {
    return pricePerUnit;
  }

  public void setPricePerUnit(Double pricePerUnit) {
    this.pricePerUnit = pricePerUnit;
  }

  public CountryModel getProducerCountry() {
    return producerCountry;
  }

  public void setProducerCountry(CountryModel producerCountry) {
    this.producerCountry = producerCountry;
  }

  public TypeModel getType() {
    return type;
  }

  public void setType(TypeModel type) {
    this.type = type;
  }

  public DrugModel getDrug() {
    return drug;
  }

  public void setDrug(DrugModel drug) {
    this.drug = drug;
  }

  public Integer getQuantityLeftInStock() {
    return quantityLeftInStock;
  }

  public void setQuantityLeftInStock(Integer quantityLeftInStock) {
    this.quantityLeftInStock = quantityLeftInStock;
  }
}
