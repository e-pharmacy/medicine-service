/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.medicine.manageproducts;

import com.ubt.medicine.commons.pageable.PageModel;
import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.jooq.tables.records.ProductsRecord;
import com.ubt.medicine.manageproducts.filters.ProductFilter;
import com.ubt.medicine.manageproducts.jooqmappers.ProductJooqMapper;
import com.ubt.medicine.manageproducts.models.ProductModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.SortOrder;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.PRODUCTS;
import static org.jooq.impl.DSL.field;

@Repository
public class ManageProductsRepositoryImpl implements ManageProductsRepository {
  private final DSLContext create;

  @Autowired
  public ManageProductsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public ProductModel getProductByBarCode(Integer barCode) {
    ProductsRecord productsRecord = create.selectFrom(PRODUCTS)
            .where(PRODUCTS.BARCODE.eq(barCode))
            .fetchOne();

    return ProductJooqMapper.map(productsRecord);
  }

  @Override
  public PageModel<ProductModel> getProducts(ProductFilter productFilter, PageableModel pageableModel) {
    Condition condition = productFilter.createFilterCondition();

    int totalElements = create.selectCount().from(PRODUCTS)
            .where(condition)
            .fetchOneInto(Integer.class);

    //latest first
    pageableModel.addOrders(DSL.field(PRODUCTS.CREATED_DATE).sort(SortOrder.DESC));

    List<ProductModel> productModels = ProductJooqMapper.map(
            create.select().from(PRODUCTS)
                    .where(condition)
                    .orderBy(pageableModel.getOrders())
                    .offset(pageableModel.getOffset())
                    .limit(pageableModel.getLimit())
                    .fetchInto(ProductsRecord.class)
    );

    return PageableUtils.createPageModel(productModels, totalElements);
  }

  @Override
  public ProductModel getProductById(long id) {
    return ProductJooqMapper.map(
            create.selectFrom(PRODUCTS)
                    .where(PRODUCTS.ID.eq(id))
                    .fetchOne()
    );
  }

  @Override
  public ProductModel createProduct(ProductModel productModel) {
    ProductsRecord productsRecord = ProductJooqMapper.unmap(productModel);

    return ProductJooqMapper.map(
            create.insertInto(PRODUCTS)
                    .set(productsRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public ProductModel updateProduct(long productId, ProductModel productModel) {
    ProductsRecord productsRecord = ProductJooqMapper.unmap(productModel);

    return ProductJooqMapper.map(
            create.update(PRODUCTS)
                    .set(productsRecord)
                    .where(PRODUCTS.ID.eq(productId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public ProductModel deleteProduct(long productId) {
    return ProductJooqMapper.map(
            create.delete(PRODUCTS)
                    .where(PRODUCTS.ID.eq(productId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public void updateProductPicture(long productId, String imagePath) {
    create.update(PRODUCTS)
            .set(PRODUCTS.IMAGE_PATH, imagePath)
            .where(PRODUCTS.ID.eq(productId))
            .execute();
  }
}
