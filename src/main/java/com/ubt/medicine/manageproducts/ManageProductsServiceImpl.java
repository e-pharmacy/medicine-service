/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.medicine.manageproducts;

import com.ubt.medicine.clients.PosFeignClient;
import com.ubt.medicine.clients.StockFeignClient;
import com.ubt.medicine.commons.BarcodeGenerator;
import com.ubt.medicine.commons.exceptions.NotFoundException;
import com.ubt.medicine.commons.pageable.PageModel;
import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.commons.pageable.PageableUtils;
import com.ubt.medicine.filestorage.StorageService;
import com.ubt.medicine.manageproducts.builders.PictureModelBuilder;
import com.ubt.medicine.manageproducts.filters.ProductFilter;
import com.ubt.medicine.manageproducts.modelexpanders.ProductModelExpander;
import com.ubt.medicine.manageproducts.models.PictureModel;
import com.ubt.medicine.manageproducts.models.ProductModel;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class ManageProductsServiceImpl implements ManageProductsService {
  private final ManageProductsRepository manageProductsRepository;
  private final ProductModelExpander productModelExpander;
  private final StockFeignClient stockFeignClient;
  private final StorageService storageService;
  private final PosFeignClient posFeignClient;

  @Autowired
  public ManageProductsServiceImpl(ManageProductsRepository manageProductsRepository, ProductModelExpander productModelExpander, StockFeignClient stockFeignClient, StorageService storageService, PosFeignClient posFeignClient) {
    this.manageProductsRepository = manageProductsRepository;
    this.productModelExpander = productModelExpander;
    this.stockFeignClient = stockFeignClient;
    this.storageService = storageService;
    this.posFeignClient = posFeignClient;
  }

  @Override
  public Page<ProductModel> getProducts(ProductFilter productFilter, Pageable pageable, List<String> expand) {
    PageableModel pageableModel = PageableUtils.createPageableModel(pageable);
    PageModel<ProductModel> pageModel = manageProductsRepository.getProducts(productFilter, pageableModel);

    productModelExpander.expandSubObjects(pageModel.getContent(), expand);

    return PageableUtils.newPage(pageModel.getContent(), pageableModel, pageModel.getTotalElements());
  }

  @Override
  public ProductModel getProductById(long id, List<String> expand) {
    ProductModel productModel = manageProductsRepository.getProductById(id);

    productModelExpander.expandSubObjects(productModel, expand);

    return productModel;
  }

  @Override
  public ProductModel createProduct(ProductModel productModel) {
    productModel.setBarcode(BarcodeGenerator.generate());
    return manageProductsRepository.createProduct(productModel);
  }

  @Override
  public ProductModel updateProduct(long productId, ProductModel productModel) {
    return manageProductsRepository.updateProduct(productId, productModel);
  }

  @Override
  public ProductModel deleteProduct(long productId) {
    return manageProductsRepository.deleteProduct(productId);
  }

  @Override
  public ProductModel getProductByBarcode(Integer barCode) {
    ProductModel productModel = manageProductsRepository.getProductByBarCode(barCode);

    if (productModel == null) {
      throw new NotFoundException("Invalid barcode! Product not found.");
    }

    Integer stockQuantity = stockFeignClient.getProductQuantityByProductId(productModel.getId());
    Integer soldProductsCount = posFeignClient.getProductSalesCount(productModel.getId());
    productModel.setQuantityLeftInStock(stockQuantity - soldProductsCount);

    return productModel;
  }

  @Override
  public ResponseEntity<Resource> getProfilePicture(String filename, HttpServletRequest request) {
    Resource resource = storageService.loadAsResource(filename);

    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Disposition",
        "inline; filename=" + filename);

    // Try to determine file's content type
    String contentType = null;
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    } catch (IOException ignored) {
    }

    //Fallback to the default content type if type could not be determined
    if (contentType == null) {
      contentType = "application/octet-stream";
    }

    return ResponseEntity
        .ok()
        .headers(headers)
        .contentType(MediaType.parseMediaType(contentType))
        .body(resource);
  }

  @Override
  public PictureModel updateProfilePicture(long productId, MultipartFile file) {
    String extension = FilenameUtils.getExtension(file.getOriginalFilename());
    String filename = UUID.randomUUID() + "." + extension;

    ProductModel productModel = manageProductsRepository.getProductById(productId);

    //delete previous profile picture if it has one
    storageService.delete(productModel.getImagePath());

    // store the file in system
    storageService.store(file, filename);

    // save to db
    manageProductsRepository.updateProductPicture(productId, filename);

    return PictureModelBuilder.aPictureModel()
        .withFilename(filename)
        .withSizeInBytes(file.getSize())
        .build();
  }

  @Override
  public void deleteProfilePicture(long productId) {
    ProductModel productModel = manageProductsRepository.getProductById(productId);

    storageService.delete(productModel.getImagePath());
    manageProductsRepository.updateProductPicture(productId, null);
  }
}
