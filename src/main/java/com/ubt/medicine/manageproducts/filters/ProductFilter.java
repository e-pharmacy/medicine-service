package com.ubt.medicine.manageproducts.filters;

import com.ubt.medicine.commons.filters.BaseFilter;
import com.ubt.medicine.commons.validation.ValidationService;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

import static com.ubt.medicine.jooq.Tables.PRODUCTS;

public class ProductFilter extends BaseFilter {
  private String brandName;
  private List<Long> drugsIds;
  private List<Long> countriesIds;
  private List<Long> typesIds;
  private List<Integer> barcodes;
  private Long productId;

  public List<Integer> getBarcodes() {
    return barcodes;
  }

  public void setBarcodes(List<Integer> barcodes) {
    this.barcodes = barcodes;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public List<Long> getDrugsIds() {
    return drugsIds;
  }

  public void setDrugsIds(List<Long> drugsIds) {
    this.drugsIds = drugsIds;
  }

  public List<Long> getCountriesIds() {
    return countriesIds;
  }

  public void setCountriesIds(List<Long> countriesIds) {
    this.countriesIds = countriesIds;
  }

  public List<Long> getTypesIds() {
    return typesIds;
  }

  public void setTypesIds(List<Long> typesIds) {
    this.typesIds = typesIds;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (ValidationService.notEmpty(getIds())) {
      condition = condition.and(PRODUCTS.ID.in(getIds()));
    }
    if (ValidationService.notBlank(getBrandName())) {
      condition = condition.and(PRODUCTS.BRAND_NAME.containsIgnoreCase(getBrandName()));
    }
    if (ValidationService.notEmpty(getTypesIds())) {
      condition = condition.and(PRODUCTS.TYPE_ID.in(getTypesIds()));
    }
    if (ValidationService.notEmpty(getCountriesIds())) {
      condition = condition.and(PRODUCTS.PRODUCER_COUNTRY_ID.in(getCountriesIds()));
    }
    if (ValidationService.notEmpty(getDrugsIds())) {
      condition = condition.and(PRODUCTS.DRUG_ID.in(getDrugsIds()));
    }
    if (ValidationService.notEmpty(barcodes)) {
      condition = condition.and(PRODUCTS.BARCODE.in(barcodes));
    }
    if (productId != null) {
      condition = condition.and(PRODUCTS.ID.eq(productId));
    }

    return condition;
  }
}
