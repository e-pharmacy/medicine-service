/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.medicine.manageproducts;

import com.ubt.medicine.manageproducts.models.ProductModel;

import com.ubt.medicine.commons.pageable.PageModel;
import com.ubt.medicine.commons.pageable.PageableModel;
import com.ubt.medicine.manageproducts.filters.ProductFilter;
import com.ubt.medicine.manageproducts.models.ProductModel;

public interface ManageProductsRepository {
  ProductModel getProductByBarCode(Integer barCode);
  
  PageModel<ProductModel> getProducts(ProductFilter productFilter, PageableModel pageableModel);

  ProductModel getProductById(long id);

  ProductModel createProduct(ProductModel productModel);

  ProductModel updateProduct(long productId, ProductModel productModel);

  ProductModel deleteProduct(long productId);

  void updateProductPicture(long productId, String imagePath);
}
