/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.medicine.manageproducts;

import com.ubt.medicine.commons.validation.annotations.Image;
import com.ubt.medicine.manageproducts.models.ProductModel;

import com.ubt.medicine.manageproducts.filters.ProductFilter;
import com.ubt.medicine.manageproducts.models.PictureModel;
import com.ubt.medicine.manageproducts.models.ProductModel;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Validated
public interface ManageProductsService {

  /**
   * @param barCode
   * @return
   */
  ProductModel getProductByBarcode(Integer barCode);

  Page<ProductModel> getProducts(ProductFilter productFilter, Pageable pageable, List<String> expand);

  ProductModel getProductById(long id, List<String> expand);

  ProductModel createProduct(@Valid ProductModel productModel);

  ProductModel updateProduct(long productId, @Valid ProductModel productModel);

  ProductModel deleteProduct(long productId);

  ResponseEntity<Resource> getProfilePicture(String filename, HttpServletRequest request);

  PictureModel updateProfilePicture(long productId, @Image(message = "{image.validation}") MultipartFile file);

  void deleteProfilePicture(long productId);
}
