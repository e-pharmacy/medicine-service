package com.ubt.medicine.manageproducts.jooqmappers;

import com.ubt.medicine.commons.validation.ValidationService;
import com.ubt.medicine.manageproducts.builders.ProductModelBuilder;
import com.ubt.medicine.manageproducts.models.ProductModel;
import com.ubt.medicine.jooq.tables.records.ProductsRecord;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductJooqMapper {
  public static ProductModel map(ProductsRecord record) {
    ProductModel model = null;
    if (record != null) {
      model = ProductModelBuilder.aProductModel()
              .withId(record.getId())
              .withImagePath(record.getImagePath())
              .withProducerCountryId(record.getProducerCountryId())
              .withBrandName(record.getBrandName())
              .withBarcode(record.getBarcode())
              .withDescription(record.getDescription())
              .withPrescription(record.getPrescription())
              .withWarning(record.getWarning())
              .withTypeId(record.getTypeId())
              .withDrugId(record.getDrugId())
              .withPricePerUnit(record.getPricePerUnit().doubleValue())
              .withRecommended(record.getIsRecommended())
              .build();
    }
    return model;
  }

  public static ProductsRecord unmap(ProductModel model) {
    ProductsRecord record = new ProductsRecord();

    if (Objects.nonNull(model.getId())) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getImagePath())) {
      record.setImagePath(model.getImagePath());
    } else {
      record.setImagePath("");
    }
    if (Objects.nonNull(model.getProducerCountryId())) {
      record.setProducerCountryId(model.getProducerCountryId());
    }
    if (ValidationService.notBlank(model.getBrandName())) {
      record.setBrandName(model.getBrandName());
    }
    if (Objects.nonNull(model.getBarcode())) {
      record.setBarcode(model.getBarcode());
    }
    if (ValidationService.notBlank(model.getDescription())) {
      record.setDescription(model.getDescription());
    }
    if (ValidationService.notBlank(model.getPrescription())) {
      record.setPrescription(model.getPrescription());
    }
    if (ValidationService.notBlank(model.getWarning())) {
      record.setWarning(model.getWarning());
    }
    if (Objects.nonNull(model.getTypeId())) {
      record.setTypeId(model.getTypeId());
    }
    if (Objects.nonNull(model.getDrugId())) {
      record.setDrugId(model.getDrugId());
    }
    if (Objects.nonNull(model.getPricePerUnit())) {
      record.setPricePerUnit(BigDecimal.valueOf(model.getPricePerUnit()));
    }
    if (Objects.nonNull(model.getRecommended())) {
      record.setIsRecommended(model.getRecommended());
    }

    return record;
  }

  public static List<ProductModel> map(List<ProductsRecord> records) {
    List<ProductModel> models = new ArrayList<>();

    for (ProductsRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
