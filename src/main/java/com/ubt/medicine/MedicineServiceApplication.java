package com.ubt.medicine;

import com.ubt.medicine.filestorage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableConfigurationProperties(StorageProperties.class)
@SpringBootApplication
public class MedicineServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(MedicineServiceApplication.class, args);
  }

}
